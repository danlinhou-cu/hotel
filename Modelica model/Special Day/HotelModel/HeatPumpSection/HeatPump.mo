within HotelModel.HeatPumpSection;
model HeatPump "Complete heat pump section with everything put together"
replaceable package MediumHW =
        Buildings.Media.ConstantPropertyLiquidWater
    "Medium for condenser water"
      annotation (choicesAllMatching = true);
  parameter Modelica.SIunits.MassFlowRate mHW_flow_nominal
    "Nominal mass flow rate of water";
  parameter Modelica.SIunits.Pressure dpHW_nominal
    "Nominal pressure difference";
replaceable package MediumDW
    "Medium for domestic hot water"
      extends
      Buildings.Media.ConstantPropertyLiquidWater;
  package Example "Examples for testing cooling tower models"
    extends Modelica.Icons.ExamplesPackage;
    model CoolingTower
      import HotelModel;
       extends Modelica.Icons.Example;
      extends
          HotelModel.HeatPumpSection.HeatPump.MediumDW.Example.BaseClass.PartialCoolingTower;

      HotelModel.CoolingTowerSection.CoolingTower cooTow(
        redeclare package MediumCW = MediumCW,
        P_nominal=P_nominal,
        dTCW_nominal=dTCW_nominal,
        dTApp_nominal=dTApp_nominal,
        TWetBul_nominal=TWetBul_nominal,
        dP_nominal=dP_nominal,
        mCW_flow_nominal=mCW_flow_nominal,
        GaiPi=GaiPi,
        tIntPi=tIntPi,
        v_flow_rate=v_flow_rate,
        eta=eta) annotation (Placement(transformation(extent={{-12,-10},{8,10}})));
      Modelica.Blocks.Sources.Step On(
        height=-1,
        offset=1,
        startTime=1800)  annotation (Placement(transformation(extent={{-80,30},{-60,
                50}})));
      Modelica.Blocks.Sources.Constant TSet(k=273.15 + 29.44)
        annotation (Placement(transformation(extent={{-78,70},{-58,90}})));
      inner Modelica.Fluid.System system(T_ambient=288.15)
        annotation (Placement(transformation(extent={{60,-60},{80,-40}})));
      Modelica.Blocks.Sources.Sine TCWLeachi(
        amplitude=2.59,
        offset=273.15 + 32.03,
        freqHz=1/1800)
        annotation (Placement(transformation(extent={{-100,-10},{-80,10}})));
      Buildings.Fluid.Sources.MassFlowSource_T souCW(
        nPorts=1,
        use_T_in=true,
        m_flow=mCW_flow_nominal,
        redeclare package Medium = MediumCW,
        T=273.15 + 29.44) "Source for CW"
        annotation (Placement(transformation(extent={{-60,-30},{-40,-10}})));
      Buildings.Fluid.Sources.FixedBoundary sinCW(redeclare package Medium =
                   MediumCW, nPorts=1) "Sink for CW" annotation (Placement(
            transformation(
            extent={{10,-10},{-10,10}},
            rotation=0,
            origin={70,0})));
      Buildings.Fluid.Sensors.TemperatureTwoPort TCWLea(redeclare package
            Medium =
            MediumCW, m_flow_nominal=mCW_flow_nominal)
        annotation (Placement(transformation(extent={{24,-10},{44,10}})));
      Modelica.Blocks.Sources.Sine TWetBulb(
        amplitude=10,
        freqHz=1/1800,
        offset=297.4) "Changing TwetBulb data represented by sine wave"
        annotation (Placement(transformation(extent={{-60,-60},{-40,-40}})));
    equation
      connect(On.y, cooTow.On) annotation (Line(
          points={{-59,40},{-38,40},{-38,4},{-14,4}},
          color={0,0,127},
          smooth=Smooth.None,
          pattern=LinePattern.Dash));
      connect(TSet.y, cooTow.TSet) annotation (Line(
          points={{-57,80},{-24,80},{-24,8},{-14,8}},
          color={0,0,127},
          smooth=Smooth.None,
          pattern=LinePattern.Dash));
      connect(TCWLeachi.y, souCW.T_in) annotation (Line(
          points={{-79,0},{-72,0},{-72,-16},{-62,-16}},
          color={0,0,127},
          smooth=Smooth.None,
          pattern=LinePattern.Dash));
      connect(souCW.ports[1], cooTow.port_a_CW) annotation (Line(
          points={{-40,-20},{-30,-20},{-30,0},{-12,0}},
          color={0,127,255},
          smooth=Smooth.None,
          thickness=1));
      connect(cooTow.port_b_CW, TCWLea.port_a) annotation (Line(
          points={{8,0},{24,0}},
          color={0,127,255},
          smooth=Smooth.None,
          thickness=1));
      connect(TCWLea.port_b, sinCW.ports[1]) annotation (Line(
          points={{44,0},{60,0}},
          color={0,127,255},
          smooth=Smooth.None,
          thickness=1));
      connect(TWetBulb.y, cooTow.TWetBul) annotation (Line(
          points={{-39,-50},{-20,-50},{-20,-4},{-14,-4}},
          color={0,0,127},
          smooth=Smooth.None,
          pattern=LinePattern.Dash));
      annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
                -100},{100,100}}), graphics), experiment(StopTime=360000));
    end CoolingTower;

    model CoolingTowerSystem
      import HotelModel;
      extends Modelica.Icons.Example;
      extends
          HotelModel.HeatPumpSection.HeatPump.MediumDW.Example.BaseClass.PartialCoolingTower;
      parameter Modelica.SIunits.Temperature TSet = 273.15+20
        "Temperture set point for CW water";
      parameter Real Motor_eta[:] = {1} "Motor efficiency";
      parameter Real Hydra_eta[:] = {1} "Hydraulic efficiency";

      Modelica.Blocks.Sources.Sine TCWLeachi(
        freqHz=1/86400,
        amplitude=2.59,
        offset=273.15 + 32.03)
        annotation (Placement(transformation(extent={{-100,0},{-80,20}})));
      Modelica.Blocks.Sources.Constant TWetBul(k=273.15 + 25)   annotation (Placement(transformation(extent={{-80,-40},
                {-60,-20}})));
      inner Modelica.Fluid.System system(T_ambient=288.15)
        annotation (Placement(transformation(extent={{60,-80},{80,-60}})));
      Buildings.Fluid.Sensors.TemperatureTwoPort TCWLea(redeclare package
            Medium =
            MediumCW, m_flow_nominal=mCW_flow_nominal)
        annotation (Placement(transformation(extent={{38,-10},{58,10}})));
      Buildings.Fluid.Sources.FixedBoundary sinCW(redeclare package Medium =
                   MediumCW, nPorts=1) "Sink for CW" annotation (Placement(
            transformation(
            extent={{10,-10},{-10,10}},
            rotation=0,
            origin={86,0})));
      HotelModel.CoolingTowerSection.CoolingTowerSystem cooTow(
        redeclare package MediumCW = MediumCW,
        P_nominal=P_nominal,
        dTCW_nominal=dTCW_nominal,
        dTApp_nominal=dTApp_nominal,
        TWetBul_nominal=TWetBul_nominal,
        dP_nominal=dP_nominal,
        mCW_flow_nominal=mCW_flow_nominal,
        GaiPi=GaiPi,
        tIntPi=tIntPi,
        v_flow_rate=v_flow_rate,
        eta=eta,
        TSet=TSet,
        Motor_eta=Motor_eta,
        Hydra_eta=Hydra_eta)
        annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
      Modelica.Blocks.Sources.CombiTimeTable sta(table=[0.0, 1; 40, 2; 80, 3; 120,
            4; 160, 5; 200, 6; 240, 7])
        annotation (Placement(transformation(extent={{-100,40},{-80,60}})));
      Modelica.Blocks.Math.RealToInteger realToInteger
        annotation (Placement(transformation(extent={{-60,40},{-40,60}})));
      Buildings.Fluid.Sources.Boundary_pT bou(
        nPorts=1,
        use_T_in=true,
        redeclare package Medium = MediumCW)
        annotation (Placement(transformation(extent={{-60,-10},{-40,10}})));
    equation
      connect(TCWLea.port_b, sinCW.ports[1]) annotation (Line(
          points={{58,0},{76,0}},
          color={0,127,255},
          smooth=Smooth.None,
          thickness=1));
      connect(TWetBul.y, cooTow.TWet) annotation (Line(
          points={{-59,-30},{-20,-30},{-20,-5.8},{-11.2,-5.8}},
          color={0,0,127},
          smooth=Smooth.None,
          pattern=LinePattern.Dash));
      connect(cooTow.port_b, TCWLea.port_a) annotation (Line(
          points={{0,-10},{20,-10},{20,0},{38,0}},
          color={0,127,255},
          smooth=Smooth.None,
          thickness=1));
      connect(sta.y[1], realToInteger.u) annotation (Line(
          points={{-79,50},{-62,50}},
          color={0,0,127},
          smooth=Smooth.None,
          pattern=LinePattern.Dash));
      connect(realToInteger.y, cooTow.sta) annotation (Line(
          points={{-39,50},{-28,50},{-28,6},{-12,6}},
          color={255,127,0},
          smooth=Smooth.None,
          pattern=LinePattern.Dash));
      connect(bou.ports[1], cooTow.port_a) annotation (Line(
          points={{-40,0},{-20,0},{-20,9.8},{0,9.8}},
          color={0,127,255},
          smooth=Smooth.None,
          thickness=1));
      connect(TCWLeachi.y, bou.T_in) annotation (Line(
          points={{-79,10},{-72,10},{-72,4},{-62,4}},
          color={0,0,127},
          smooth=Smooth.None,
          pattern=LinePattern.Dash));
      annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
                -100},{100,100}}), graphics));
    end CoolingTowerSystem;

    package BaseClass
      model PartialCoolingTower "Partial model for cooling tower examples"
       package MediumCW = Buildings.Media.ConstantPropertyLiquidWater
          "Medium for condenser water"
            annotation (choicesAllMatching = true);
       parameter Modelica.SIunits.Power P_nominal=30000
          "Nominal compressor power (at y=1)";
        parameter Modelica.SIunits.TemperatureDifference dTCW_nominal=5.18
          "Temperature difference between the outlet and inlet of the modules";
        parameter Modelica.SIunits.TemperatureDifference dTApp_nominal=4.44
          "Nominal approach temperature";
        parameter Modelica.SIunits.Temperature TWetBul_nominal=273.15+25
          "Nominal wet bulb temperature";
        parameter Modelica.SIunits.Pressure dP_nominal=1000
          "Pressure difference between the outlet and inlet of the modules ";
        parameter Modelica.SIunits.MassFlowRate mCW_flow_nominal=10
          "Nominal mass flow rate at condenser water wide";
        parameter Real GaiPi=1 "Gain of the PI controller";
        parameter Real tIntPi=60 "Integration time of the PI controller";
        parameter Real v_flow_rate[:]={1} "Volume flow rate rate";
        parameter Real eta[:]={1} "Fan efficiency";

      end PartialCoolingTower;
    end BaseClass;
  end Example;
end MediumDW;
      //Buildings.Media.Interfaces.PartialSimpleMedium
   parameter Modelica.SIunits.MassFlowRate mDW_flow_nominal
    "Nominal mass flow rate";
    //Nominal flow rate is value I gave, probably different
   parameter Modelica.SIunits.Pressure dpDW_nominal
    "Nominal pressure difference";
  parameter Modelica.SIunits.Power Q_flow_nominal "Nominal heat flow";
  parameter Modelica.SIunits.Volume HeatPumpVol "Volume of the Heat Pump";
  parameter Modelica.SIunits.Temperature HeaPumTRef
    "Reference tempearture of heat pump";
  parameter Modelica.SIunits.Temp_K TSetBoiIn "Set temperature for boiler";
  BoilerPackage.Boiler2WithControls HeaPumBoi(
    redeclare package MediumHW = MediumHW,
    dpHW_nominal=dpHW_nominal,
    Q_flow_nominal=Q_flow_nominal,
    TSetBoiIn=TSetBoiIn,
    mHW_flow_nominal=mHW_flow_nominal,
    bolCon(conPID(controllerType=Modelica.Blocks.Types.SimpleController.PI, Ti=
            60))) "Boiler corresponding to the heat pump section"
    annotation (Placement(transformation(extent={{-20,42},{-60,78}})));
  HeatPumpPackage.HeatPumpwithControls HeaPum(
    redeclare package MediumHW = MediumHW,
    dpHW_nominal=dpHW_nominal,
    HeatPumpVol=HeatPumpVol,
    HeaPumTRef=HeaPumTRef,
    mHW_flow_nominal=mHW_flow_nominal) "Heat Pump"
    annotation (Placement(transformation(extent={{-60,-80},{-20,-40}})));
  HeatExchangeValvesPackage.HexValves_with_Control HexVal(
    redeclare package MediumDW = MediumDW,
    mDW_flow_nominal=mDW_flow_nominal,
    mHW_flow_nominal=mHW_flow_nominal,
    redeclare package MediumHW = MediumHW,
    dpHW_nominal=dpHW_nominal,
    dpDW_nominal=dpDW_nominal) "Heat exchange valves with controls"
                                                                annotation (
     Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=90,
        origin={40,0})));

  Modelica.Fluid.Interfaces.FluidPort_a port_a1(redeclare package Medium =
        MediumHW)
    "Fluid connector a (positive design flow direction is from port_a to port_b)"
    annotation (Placement(transformation(extent={{-110,50},{-90,70}})));
  Modelica.Fluid.Interfaces.FluidPort_b port_b1(redeclare package Medium =
        MediumHW)
    "Fluid connector b (positive design flow direction is from port_a to port_b)"
    annotation (Placement(transformation(extent={{-110,-70},{-90,-50}})));
  Modelica.Blocks.Interfaces.IntegerInput Sta
    "States controlled by the supervisory control"
    annotation (Placement(transformation(extent={{-120,10},{-100,30}})));
  Modelica.Fluid.Interfaces.FluidPort_a port_a2(redeclare package Medium =
        MediumDW)
    "Fluid connector a2 (positive design flow direction is from port_a2 to port_b2)"
    annotation (Placement(transformation(extent={{90,-70},{110,-50}})));
  Modelica.Fluid.Interfaces.FluidPort_b port_b2(redeclare package Medium =
        MediumDW)
    "Fluid connector b2 (positive design flow direction is from port_a2 to port_b2)"
    annotation (Placement(transformation(extent={{90,50},{110,70}})));
  Modelica.Blocks.Interfaces.RealOutput Tboi
    "Temperature of the passing fluid in the boiler" annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={60,110})));
  Modelica.Blocks.Interfaces.RealOutput THeaPum
    "Temperature of the passing fluid leaving from the heat pumps"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={60,-110})));
  Modelica.Blocks.Interfaces.RealInput Q_flow1 "Heat Flow input "
    annotation (Placement(transformation(extent={{-140,-40},{-100,0}})));
  Modelica.Blocks.Interfaces.RealOutput BypValPos "Actual valve position"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={40,110})));
equation
  connect(HexVal.port_a1, HeaPum.port_b1) annotation (Line(
      points={{32,-20},{32,-60},{-20,-60}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(HeaPumBoi.Sta, Sta) annotation (Line(
      points={{-18.4,71.16},{-18.4,72},{-8,72},{-8,20},{-110,20}},
      color={255,127,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash));
  connect(HeaPum.THeaPum, THeaPum) annotation (Line(
      points={{-18,-52},{0,-52},{0,-40},{60,-40},{60,-110}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash));
  connect(HeaPum.THeaPum, HeaPumBoi.HPTSen) annotation (Line(
      points={{-18,-52},{0,-52},{0,67.56},{-18.4,67.56}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash));
  connect(HeaPumBoi.TBoi, Tboi) annotation (Line(
      points={{-62.4,67.56},{-62.4,68},{-80,68},{-80,40},{60,40},{60,110}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash));

  connect(Sta, HexVal.Sta) annotation (Line(
      points={{-110,20},{-8,20},{-8,1.33227e-015},{17.6,1.33227e-015}},
      color={255,127,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash));
  connect(HeaPum.Q_flow1, Q_flow1) annotation (Line(
      points={{-64,-68},{-80,-68},{-80,-20},{-120,-20}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash));
  connect(HexVal.port_b1, HeaPumBoi.port_a1) annotation (Line(
      points={{32,20},{32,60},{-20,60}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(HexVal.port_b2, port_a2) annotation (Line(
      points={{48.4,-20},{48,-20},{48,-60},{100,-60}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(port_b2, HexVal.port_a2) annotation (Line(
      points={{100,60},{48,60},{48,20}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(HeaPumBoi.port_b1, port_a1) annotation (Line(
      points={{-60,60},{-100,60}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(HeaPum.port_a1, port_b1) annotation (Line(
      points={{-60.4,-60},{-100,-60}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(HexVal.BypValPos, BypValPos) annotation (Line(
      points={{40,22},{40,110}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}),
        graphics={
        Rectangle(
          extent={{-80,80},{80,-80}},
          lineThickness=1,
          pattern=LinePattern.None,
          lineColor={135,135,135},
          fillColor={175,175,175},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-80,60},{80,20}},
          lineColor={255,0,0},
          fillColor={255,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-80,-20},{80,-60}},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          lineColor={0,0,0}),
        Rectangle(
          extent={{-80,20},{80,-20}},
          fillColor={128,0,255},
          fillPattern=FillPattern.HorizontalCylinder,
          pattern=LinePattern.None,
          lineColor={0,0,0})}));
end HeatPump;
