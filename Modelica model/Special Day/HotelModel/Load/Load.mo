within HotelModel.Load;
model Load

  Modelica.Blocks.Interfaces.RealInput TDryBul
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
  Modelica.Blocks.Interfaces.RealOutput Loa
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));
  Modelica.Blocks.Sources.RealExpression PurLoad(y=max((TDryBul - 21 - 273.15)/5.5
        *0.5*410000, -350000) + 0.5*410000)
    "Load calculated from outdoor dry bulb temperature"
    annotation (Placement(transformation(extent={{-42,10},{-22,30}})));
  Modelica.Blocks.Sources.RealExpression ComHeaHP(y=abs(PurLoad.y/5.3))
    "Compressor heat of the heat pump"
    annotation (Placement(transformation(extent={{-42,-30},{-22,-10}})));
  Modelica.Blocks.Math.Add add
    annotation (Placement(transformation(extent={{0,-10},{20,10}})));
equation
 // Loa=max((TDryBul-21-273.15)/5.5*0.5*410000,-350000)+0.5*410000
 // annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
 //          -100},{100,100}}), graphics));
  connect(PurLoad.y, add.u1) annotation (Line(points={{-21,20},{-12,20},{-12,6},
          {-2,6}}, color={0,0,127}));
  connect(ComHeaHP.y, add.u2) annotation (Line(points={{-21,-20},{-12,-20},{-12,
          -6},{-2,-6}}, color={0,0,127}));
  connect(add.y, Loa)
    annotation (Line(points={{21,0},{110,0}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},
            {100,100}}), graphics={Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,127},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid), Text(
          extent={{-156,146},{144,106}},
          textString="%name",
          lineColor={0,0,255})}));
end Load;
