# -*- coding: utf-8 -*-
"""
Created on Sun Sep 10 23:48:45 2017

@author: Danlin
"""

import xlrd
import matplotlib.pyplot as plt
import pylab 

file_location="C:\Users\Danlin\Desktop\consumption_hotel.xlsx"
workbook=xlrd.open_workbook(file_location)
first_sheet=workbook.sheet_by_index(0)

time=first_sheet.col_values(1, 1)
state=first_sheet.col_values(2, 1)
load=first_sheet.col_values(3, 1)
pump_CT=first_sheet.col_values(4, 1)
boiler=first_sheet.col_values(5, 1)

plt.figure(1)
plt.ylabel('State')
pylab.plot(time,state)
plt.axis([0,23,5,7.2])
plt.xlabel('Time[h]')
plt.figure(2)
plt.ylabel('Load [w]')
pylab.plot(time,load)
plt.axis([0,23,120000,300000])
plt.xlabel('Time[h]')
plt.figure(3)
plt.ylabel('Consumption of Pump_CT [W]')
plt.axis([0,23,0,20000])
pylab.plot(time,pump_CT)
plt.xlabel('Time[h]')
plt.figure(4)
plt.ylabel('Consumption of Boiler [m3/s]')
pylab.plot(time,boiler)
plt.axis([0,23,0,0.01])
plt.xlabel('Time[h]')
plt.show()
