within ;
package HotelModel "Models of hotel project"
  annotation (uses(
        Buildings(version="1.6"),
    Hotel2(version="2"),
    Modelica(version="3.2.2"),
    Modelica_StateGraph2(version="2.0.3")),
      version="2",
    conversion(from(version="1", script="ConvertFromNewHotelModel_1.mos")));
end HotelModel;
