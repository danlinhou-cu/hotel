within HeatRecoverySystem;
model HotelModel_annual_baseline1
     extends Modelica.Icons.Example;
     replaceable package MediumCW =
      Buildings.Media.ConstantPropertyLiquidWater "Medium for condenser water"
      annotation (choicesAllMatching = true);
  parameter Modelica.SIunits.MassFlowRate mCW_flow_nominal=29.3
    "Nominal mass flow rate of water";
  parameter Modelica.SIunits.Pressure dp_nominal=1000
    "Nominal pressure difference";
       replaceable package MediumDW =
      Buildings.Media.ConstantPropertyLiquidWater
    "Medium for domestic hot water";
      //Buildings.Media.Interfaces.PartialSimpleMedium
   parameter Modelica.SIunits.MassFlowRate mDW_flow_nominal=7
    "Nominal mass flow rate";                                 //12.62*3
      //The nominal flow rate of water for domestic flow rate is one I gave it
      //Need to look up actual values
   parameter Modelica.SIunits.Pressure dpDW_nominal=1000
    "Nominal pressure difference";
  replaceable package MediumHW =
      Buildings.Media.ConstantPropertyLiquidWater "Medium for condenser water"
      annotation (choicesAllMatching = true);
  parameter Modelica.SIunits.MassFlowRate mHW_flow_nominal=29.3
    "Nominal mass flow rate of water";
  parameter Modelica.SIunits.Pressure dpHW_nominal=1000
    "Nominal pressure difference";
  parameter String resultFileName1 = "result1.txt"
    "File on which data is present";
  parameter String resultFileName2 = "result2.txt"
    "File on which data is present";
  CoolingTowerSection.CoolingTowerSystem coolingTowerSystem(
    redeclare package MediumCW = MediumCW,
    Motor_eta={0.85},
    Hydra_eta={1},
    GaiPi=1,
    mCW_flow_nominal=mCW_flow_nominal,
    v_flow_rate={0,0.1,0.3,0.6,1},
    eta={0,0.1^3,0.3^3,0.6^3,1},
    tIntPi=60,
    dTCW_nominal(displayUnit="degC") = 5.56,
    dTApp_nominal(displayUnit="degC") = 3.89,
    TWetBul_nominal=273.15 + 25.55,
    TSet=273.15 + 29.44,
    coolingTower(val(riseTime=60)),
    P_nominal=2237,
    Pum(
      motorEfficiency(eta={0.7}),
      hydraulicEfficiency(eta={0.7}),
      filteredSpeed=true),
    dP_nominal(displayUnit="Pa") = 127266.7)                annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-90,26})));
  HeatPumpSection.HeatPump heatPump(
    redeclare package MediumDW = MediumDW,
    HeatPumpVol=25.99029318,
    dpDW_nominal=dpDW_nominal,
    redeclare package MediumHW = MediumHW,
    mHW_flow_nominal=mHW_flow_nominal,
    dpHW_nominal=dpDW_nominal,
    mDW_flow_nominal=mHW_flow_nominal,
    Q_flow_nominal=1024283.3902519,
    HeaPum(HeaPum(HeaPumTan(T_start=273.15 + 20)), dpHW_nominal(displayUnit=
            "Pa") = 74126),
    HexVal(Hex(hexval1(riseTime=60, dpValve_nominal=6894),
                                     valbyp(riseTime=60, dpValve_nominal=6894),
        hex(dp1_nominal=67017, dp2_nominal=5636)), dpHW_nominal(displayUnit=
            "Pa") = 0),
    HeaPumTRef=273.15 + 22.22,
    TSetBoiIn=273.15 + 16.11,
    HeaPumBoi(dpHW_nominal(displayUnit="Pa") = 72179.54))
    annotation (Placement(transformation(extent={{14,18},{34,-2}})));

  ConnectingPackage.ConnectingLoop connectingLoop(redeclare package MediumDW =
        MediumDW,
    dpDW_nominal=dpDW_nominal,
    mDW_flow_nominal=mDW_flow_nominal,
    pum(filteredSpeed=false,
      addPowerToMedium=false,
      motorCooledByFluid=false),
    pum1(filteredSpeed=false),
    val4(dpValve_nominal=dpDW_nominal),
    pum2(addPowerToMedium=false))
    annotation (Placement(transformation(extent={{80,-24},{100,-4}})));
  DomesticHotWater.DomesticWaterControls domesticWaterControls(redeclare
      package MediumDW = MediumDW,
    VTan=3,
    hTan=3,
    MassFloKitIn=0.03,
    dIns=0.05,
    mDW_flow_nominal=mDW_flow_nominal,
    domesticHotWaterSystem(cooWatCon(
        kPCon=0.1,
        TDomHotWatSet=316.15,
        conPID(yMax=1))),
    dpDW_nominal=dpDW_nominal,
    TBoiSetIn=273.15 + 60,
    MassFloDomIn=8,
    Q_flow_DWnominal=408352)
    annotation (Placement(transformation(extent={{140,-40},{160,-20}})));
  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{-180,-120},{-160,-100}})));
  Buildings.Fluid.HeatExchangers.ConstantEffectiveness hex(
    redeclare package Medium1 = MediumCW,
    redeclare package Medium2 = MediumHW,
    m1_flow_nominal=mCW_flow_nominal,
    m2_flow_nominal=mHW_flow_nominal,
    dp1_nominal=67017,
    dp2_nominal=68672)                                     annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-50,10})));
  Buildings.Fluid.Storage.ExpansionVessel exp3(
    redeclare package Medium = MediumCW,
    V_start=1)
              annotation (Placement(transformation(extent={{-24,-18},{-16,-10}})));
  Buildings.Fluid.Storage.ExpansionVessel exp4(
    redeclare package Medium = MediumCW,
    V_start=1)
              annotation (Placement(transformation(extent={{-94,48},{-86,56}})));
  Modelica.Blocks.Sources.CombiTimeTable KitHotWatDem(
    fileName="Twb1.txt",
    tableName="table1",
    tableOnFile=false,
    smoothness=Modelica.Blocks.Types.Smoothness.ContinuousDerivative,
    table=[0,0.21; 3600,0.21; 7200,0.21; 10800,0.21; 14400,0.21; 18000,0.21;
        21600,0.21; 25200,0.21; 28800,0.21; 32400,0.21; 36000,0.21; 39600,1.15;
        43200,0.21; 46800,0.21; 50400,0.21; 54000,0.21; 57600,0.21; 61200,0.21;
        64800,0.21; 68400,1.15; 72000,0.21; 75600,0.21; 79200,0.21; 82800,0.21;
        86400,0.21],
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic)
    annotation (Placement(transformation(extent={{52,-70},{60,-62}})));
  Buildings.Fluid.Storage.ExpansionVessel exp2(
    redeclare package Medium = MediumCW, V_start=10)
              annotation (Placement(transformation(extent={{136,-2},{144,6}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaData(filNam=
        "C:/Users/Danlin/Documents/Bitbucket/Hotel/Modelica model/Real Case/HeatRecoverySystem/Resources/weatherdata/USA_FL_Miami.Intl.AP.722020_TMY.mos")
    annotation (Placement(transformation(extent={{-166,16},{-156,24}})));
  Buildings.BoundaryConditions.WeatherData.Bus
                                     weaBus
    annotation (Placement(transformation(extent={{-150,10},{-130,30}})));
  Modelica.Blocks.Sources.CombiTimeTable GueRooDomWatDem(
    fileName="Twb1.txt",
    tableName="table1",
    tableOnFile=false,
    smoothness=Modelica.Blocks.Types.Smoothness.ContinuousDerivative,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    table=[0,1.2; 3600,0.948623746; 7200,0.534541952; 10800,0.361380475; 14400,0.368909235;
        18000,0.549599472; 21600,0.66253087; 25200,4.607601052; 28800,5.217430603;
        32400,1.242245382; 36000,0.617358311; 39600,0.910979947; 43200,0.632415831;
        46800,0.504426913; 50400,0.752875989; 54000,0.70017467; 57600,0.534541952;
        61200,0.090345119; 64800,0.70770343; 68400,0.918508706; 72000,0.782991028;
        75600,0.828163588; 79200,1.069083904; 82800,1.475636938; 86400,1.204601582])
    annotation (Placement(transformation(extent={{52,-54},{60,-46}})));
  Modelica.Blocks.Sources.CombiTimeTable Load(
    smoothness=Modelica.Blocks.Types.Smoothness.ContinuousDerivative,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    table=[0,0.0; 3600,0.0; 7200,0.0; 10800,0.0; 14400,0.0; 18000,0; 21600,0;
        25200,1.5; 28800,1.5; 32400,1.5; 36000,0; 39600,0; 43200,0.0; 46800,0.0;
        50400,0.0; 54000,0; 57600,0; 61200,1.5; 64800,1.5; 68400,1.5; 72000,0.0;
        75600,0.0; 79200,0.0; 82800,0.0; 86400,0.0],
    tableOnFile=true,
    tableName="tab1",
    fileName="C:/Users/Danlin/Documents/Dymola/Hotel/V4-Danlin/HotelModel/load1.txt")
    annotation (Placement(transformation(extent={{-58,-44},{-50,-36}})));
  Modelica.Blocks.Sources.CombiTimeTable KitHotWatDem1(
    fileName="Twb1.txt",
    tableName="table1",
    tableOnFile=false,
    smoothness=Modelica.Blocks.Types.Smoothness.ContinuousDerivative,
    table=[0,0.21; 3600,0.21; 7200,0.21; 10800,0.21; 14400,0.21; 18000,0.21;
        21600,0.21; 25200,0.21; 28800,0.21; 32400,0.21; 36000,0.21; 39600,1.15;
        43200,0.21; 46800,0.21; 50400,0.21; 54000,0.21; 57600,0.21; 61200,0.21;
        64800,0.21; 68400,1.15; 72000,0.21; 75600,0.21; 79200,0.21; 82800,0.21;
        86400,0.21],
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic)
    annotation (Placement(transformation(extent={{52,-70},{60,-62}})));
  Buildings.Fluid.Storage.ExpansionVessel exp1(
    V_start=1, redeclare package Medium = MediumDW)
              annotation (Placement(transformation(extent={{54,-16},{62,-8}})));
  Modelica.Blocks.Sources.IntegerExpression supCon(y=7)
    annotation (Placement(transformation(extent={{-166,70},{-146,90}})));
  Modelica.Blocks.Sources.RealExpression HRPump(y=heatPump.HeaPum.HeaPum.Pum.P)
    "Power consumption of the pump working for the heatpum"
    annotation (Placement(transformation(extent={{190,20},{210,40}})));
  Modelica.Blocks.Continuous.Integrator HRPumpEne
    "Total power consumption of the pump working for the heatpum"
    annotation (Placement(transformation(extent={{220,20},{240,40}})));
  Modelica.Blocks.Sources.RealExpression cooTowPum(y=coolingTowerSystem.Pum.P)
    "Power consumption of the pump working for the cooling tower"
    annotation (Placement(transformation(extent={{190,80},{210,100}})));
  Modelica.Blocks.Continuous.Integrator cooTowPumEne
    "Total power consumption of the pump working for the cooling tower"
    annotation (Placement(transformation(extent={{220,80},{240,100}})));
  Modelica.Blocks.Sources.RealExpression cooTow(y=coolingTowerSystem.coolingTower.P)
    "Power consumption of the cooling tower"
    annotation (Placement(transformation(extent={{190,50},{210,70}})));
  Modelica.Blocks.Continuous.Integrator cooTowEne
    "Total power consumption of the cooling tower"
    annotation (Placement(transformation(extent={{220,50},{240,70}})));
  Modelica.Blocks.Sources.RealExpression DHWBoi(y=domesticWaterControls.domesticHotWaterSystem.boi.VFue_flow)
    "Heat of the boiler working for the DHW system"
    annotation (Placement(transformation(extent={{190,-70},{210,-50}})));
  Modelica.Blocks.Continuous.Integrator DHWBoiEne
    annotation (Placement(transformation(extent={{220,-70},{240,-50}})));
  Modelica.Blocks.Sources.RealExpression makUpWat(y=coolingTowerSystem.coolingTower.yorkCalc.Q_flow
        /(2260*1000) - coolingTowerSystem.coolingTower.yorkCalc.port_a.m_flow*
        0.003) "Make up water for the cooling tower [kg/s]"
    annotation (Placement(transformation(extent={{190,-100},{210,-80}})));
  Modelica.Blocks.Continuous.Integrator makUpWatTot
    "Total amount of makeupwater for the cooling tower"
    annotation (Placement(transformation(extent={{220,-100},{240,-80}})));
  Modelica.Blocks.Sources.RealExpression CT_rej(y=-coolingTowerSystem.coolingTower.yorkCalc.Q_flow)
    "heat reject of cooling tower"
    annotation (Placement(transformation(extent={{190,-130},{210,-110}})));
  Modelica.Blocks.Continuous.Integrator CT_rej_Tot
    "total amount of heat reject of cooling tower"
    annotation (Placement(transformation(extent={{220,-130},{240,-110}})));
  Modelica.Blocks.Continuous.Integrator HeaRecTot
    "total amount of heat recovery"
    annotation (Placement(transformation(extent={{160,-130},{180,-110}})));
  Modelica.Blocks.Sources.RealExpression HeaRec(y=heatPump.HexVal.Hex.hex.Q1_flow)
    "heat recovery "
    annotation (Placement(transformation(extent={{130,-130},{150,-110}})));
  Modelica.Blocks.Continuous.Integrator integrator
    annotation (Placement(transformation(extent={{160,50},{180,70}})));
  Modelica.Blocks.Sources.RealExpression realExpression2(y=if supCon.y > 4
         then connectingLoop.MasFloRatCloWat.m_flow*(60 - 20)*4.2 else 0)
    annotation (Placement(transformation(extent={{130,50},{150,70}})));
  Modelica.Blocks.Continuous.Integrator integrator1
    annotation (Placement(transformation(extent={{160,80},{180,100}})));
  Modelica.Blocks.Sources.RealExpression realExpression1(y=if supCon.y > 4
         then max((connectingLoop.senTem.T - 273.15 - 20)*(connectingLoop.MasFloRatCloWat.m_flow)
        *4.2*(1 - heatPump.BypValPos), 0) else 0)
    annotation (Placement(transformation(extent={{130,80},{150,100}})));
  Modelica.Blocks.Sources.RealExpression realExpression(y=if supCon.y > 4 then
        max((connectingLoop.senTem.T - 273.15 - 20)*(connectingLoop.MasFloRatCloWat.m_flow)
        *4.2*(1 - heatPump.BypValPos)/((connectingLoop.MasFloRatCloWat.m_flow)*(
        60 - 20)*4.2 + 0.00001), 0) else 0)
    annotation (Placement(transformation(extent={{100,80},{120,100}})));
  Modelica.Blocks.Sources.RealExpression CONPump(y=connectingLoop.pum.P +
        connectingLoop.pum1.P + connectingLoop.pum2.P)
    "Power consumption of the pump working for the connecting system"
    annotation (Placement(transformation(extent={{190,-10},{210,10}})));
  Modelica.Blocks.Continuous.Integrator CONPumpEne
    "Total power consumption of the pump working for the connecting system"
    annotation (Placement(transformation(extent={{220,-10},{240,10}})));
  Modelica.Blocks.Sources.RealExpression DHWPump(y=domesticWaterControls.domesticHotWaterSystem.pum.P
         + domesticWaterControls.domesticHotWaterSystem.KitPum.P +
        domesticWaterControls.domesticHotWaterSystem.DomPum.P)
    "Power consumption of the pump working for the DHW system"
    annotation (Placement(transformation(extent={{190,-40},{210,-20}})));
  Modelica.Blocks.Continuous.Integrator DHWPumpEne1
    "Total power consumption of the pump working for the DHW system"
    annotation (Placement(transformation(extent={{220,-40},{240,-20}})));
equation
  when sample(0,3600) then
     Modelica.Utilities.Streams.print(realString(number=integrator1.y, minimumWidth=1, precision=16), resultFileName1);
     Modelica.Utilities.Streams.print(realString(number=integrator.y, minimumWidth=1, precision=16), resultFileName2);
  end when;
  connect(connectingLoop.port_a1, domesticWaterControls.port_a1) annotation (
      Line(
      points={{100,-8},{140,-8},{140,-30}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(connectingLoop.port_b1, domesticWaterControls.port_a2) annotation (
      Line(
      points={{100.2,-20},{100,-20},{100,-40},{150,-40}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(coolingTowerSystem.port_a, hex.port_b1) annotation (Line(
      points={{-90,35.8},{-90,40},{-56,40},{-56,20}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(coolingTowerSystem.port_b, hex.port_a1) annotation (Line(
      points={{-90,16},{-90,-20},{-56,-20},{-56,0}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(hex.port_a2, heatPump.port_b1) annotation (Line(
      points={{-44,20},{-44,40},{14,40},{14,14}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(hex.port_b2, heatPump.port_a1) annotation (Line(
      points={{-44,0},{-44,-20},{14,-20},{14,2}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(heatPump.port_a2, connectingLoop.port_b2) annotation (Line(
      points={{34,14},{34,40},{80,40},{80,-8}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(heatPump.port_b2, connectingLoop.port_a2) annotation (Line(
      points={{34,2},{34,-20},{80,-20}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(exp4.port_a, hex.port_b1) annotation (Line(
      points={{-90,48},{-90,40},{-56,40},{-56,20}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(domesticWaterControls.Pum3_flow, connectingLoop.m_flow_in)
    annotation (Line(
      points={{161,-29.4},{176,-29.4},{176,18},{74,18},{74,-16},{78,-16}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash));
  connect(KitHotWatDem.y[1], domesticWaterControls.m_flow_in_kit) annotation (
      Line(
      points={{60.4,-66},{128,-66},{128,-26},{138,-26}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash));
  connect(exp3.port_a, heatPump.port_a1) annotation (Line(
      points={{-20,-18},{-20,-20},{14,-20},{14,2}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));

  connect(exp2.port_a, domesticWaterControls.port_a1) annotation (Line(
      points={{140,-2},{140,-30}},
      color={0,127,255},
      smooth=Smooth.None,
      pattern=LinePattern.Dash));
  connect(weaData.weaBus,weaBus)  annotation (Line(
      points={{-156,20},{-140,20}},
      thickness=0.5,
      smooth=Smooth.None,
      color={0,128,255},
      pattern=LinePattern.Dash),
                           Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(weaBus.TWetBul, coolingTowerSystem.TWet) annotation (Line(
      points={{-140,20},{-122,20},{-122,20.2},{-101.2,20.2}},
      color={0,0,255},
      thickness=0.5,
      smooth=Smooth.None,
      pattern=LinePattern.Dash), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(domesticWaterControls.m_flow_in_dom, GueRooDomWatDem.y[1])
    annotation (Line(
      points={{138,-22},{114,-22},{114,-50},{60.4,-50}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash));
  connect(Load.y[1], heatPump.Q_flow1) annotation (Line(
      points={{-49.6,-40},{0,-40},{0,10},{12,10}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(exp1.port_a, connectingLoop.port_a2)
    annotation (Line(points={{58,-16},{58,-20},{80,-20}}, color={0,127,255}));
  connect(supCon.y, coolingTowerSystem.sta) annotation (Line(points={{-145,80},{
          -124,80},{-124,32},{-102,32}},  color={255,127,0}));
  connect(heatPump.Sta, coolingTowerSystem.sta) annotation (Line(points={{13,6},{
          -26,6},{-26,80},{-124,80},{-124,32},{-102,32}},  color={255,127,0},
      pattern=LinePattern.Dash));
  connect(connectingLoop.sta1, coolingTowerSystem.sta) annotation (Line(points={{86,-2},
          {86,-2},{86,80},{-124,80},{-124,32},{-102,32}},          color={255,127,
          0},
      pattern=LinePattern.Dash));
  connect(cooTowPum.y,cooTowPumEne. u)
    annotation (Line(points={{211,90},{218,90}}, color={0,0,127}));
  connect(cooTow.y,cooTowEne. u)
    annotation (Line(points={{211,60},{218,60}}, color={0,0,127}));
  connect(HRPump.y,HRPumpEne. u)
    annotation (Line(points={{211,30},{218,30}}, color={0,0,127}));
  connect(DHWBoi.y,DHWBoiEne. u)
    annotation (Line(points={{211,-60},{218,-60}}, color={0,0,127}));
  connect(makUpWat.y,makUpWatTot. u)
    annotation (Line(points={{211,-90},{218,-90}}, color={0,0,127}));
  connect(CT_rej.y,CT_rej_Tot. u)
    annotation (Line(points={{211,-120},{218,-120}},
                                                  color={0,0,127}));
  connect(realExpression1.y, integrator1.u)
    annotation (Line(points={{151,90},{158,90}}, color={0,0,127}));
  connect(realExpression2.y, integrator.u)
    annotation (Line(points={{151,60},{158,60}}, color={0,0,127}));
  connect(HeaRec.y, HeaRecTot.u)
    annotation (Line(points={{151,-120},{158,-120}}, color={0,0,127}));
  connect(supCon.y, domesticWaterControls.sta1) annotation (Line(points={{-145,80},
          {104,80},{104,-37},{138,-37}}, color={255,127,0},
      pattern=LinePattern.Dash));
  connect(CONPump.y,CONPumpEne. u)
    annotation (Line(points={{211,0},{218,0}},   color={0,0,127}));
  connect(DHWPump.y, DHWPumpEne1.u)
    annotation (Line(points={{211,-30},{218,-30}}, color={0,0,127}));
  annotation (__Dymola_Commands(file=
          "modelica://HotelModel/Resources/Scripts/HotelModel_annual.mos"
        "Simulate and plot"),
        Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-200,-140},
            {260,120}})),                 Icon(coordinateSystem(
          preserveAspectRatio=false)),
    experiment(StopTime=3.1526e+007),
    __Dymola_experimentSetupOutput,
    Documentation(info="<html>
<p>The purpose of the model is to compare the HVAC system design of a heat recovery system (HR) versus a conventional system that does not use heat recovery and evaluate both systems total energy consumption, peak energy and system stability. </p>
<p>The models and controls are being implemented in Dymola using the Modelica Building Library which is an open source library for building environment simulation. </p>
<p>The specific system being used for experimentation is a the Grand Beach Hotel in Miami Beach, Florida</p>
<h4><span style=\"color:#008000\">Hotel Schematics</span></h4>
<p><img src=\"modelica://HotelModel/Resources/HotelInfo/HotelSchematic_heating.PNG\"/></p>
<h4><span style=\"color:#008000\">System Stages</span></h4>
<p><br><img src=\"modelica://HotelModel/Resources/HotelInfo/Stage.PNG\"/></p>
<p><u><b></font><font style=\"color: #008000; \">Unit Conversions</font></b></u></p>
<p>mCW_flow_nominal = 699 GPM [44.10 kg/s]</p>
<p>dp_nominal = 4.335 psi [29889.8 Pa]</p>
<p>mDW_flow_nominal = 25.68 GPM [1.62 kg/s]</p>
<p>dpDW_flow_nominal = 4.335 psi [29889.8 Pa]</p>
<p>mHW_flow_nominal = 699 GPM [44.10 kg/s]</p>
<p>dpHW_flow_nominal = 4.335 psi [29889.8 Pa]</p>
<p><i><b><font style=\"color: #008000; \">Unit Conversion in subsystem models</font></b></i></p>
<h5>Cooling Tower System parameters</h5>
<table cellspacing=\"2\" cellpadding=\"0\" border=\"0\"><tr>
<td><h4>Name</h4></td>
<td><h4>IP units</h4></td>
<td><h4>SI units</h4></td>
</tr>
<tr>
<td><p>P_nominal&nbsp;</p></td>
<td><p>2.95 horsepower&nbsp;</p></td>
<td><p>2.2kW</p></td>
</tr>
<tr>
<td><p>dTC_nominal</p></td>
<td><p>10&deg; F</p></td>
<td><p>5.56&deg; C</p></td>
</tr>
<tr>
<td><p>dTApp_nominal</p></td>
<td><p>3&deg; F</p></td>
<td><p>3.89&deg; C</p></td>
</tr>
<tr>
<td><p>TWetBul_nominal</p></td>
<td><p>79&deg; F</p></td>
<td><p>273.15 + 25.55 K</p></td>
</tr>
<tr>
<td><p>Tset</p></td>
<td><p>85&deg; F</p></td>
<td><p>273.15+29.44 K</p></td>
</tr>
</table>
<h5>Heat Pump System parameters</h5>
<table cellspacing=\"2\" cellpadding=\"0\" border=\"0\"><tr>
<td><h4>Name</h4></td>
<td><h4>IP units</h4></td>
<td><h4>SI units</h4></td>
</tr>
<tr>
<td><p>Q_flow_nominal</p></td>
<td><p>3495 MBH</p></td>
<td><p>1024283.3902519 W</p></td>
</tr>
<tr>
<td><p>Q_floSet</p></td>
<td><p>2796 MBH&nbsp;</p></td>
<td><p>&nbsp;819426.71220153 W</p></td>
</tr>
<tr>
<td><p>HeatPumpVol</p></td>
<td><p>6865.9 gal</p></td>
<td><p>&nbsp;25.99029318 m^3</p></td>
</tr>
<tr>
<td><p>HeatPumTRef</p></td>
<td><p>72&deg; F</p></td>
<td><p>273.15 + 22.22 K</p></td>
</tr>
<tr>
<td><p>TSetBoiIn</p></td>
<td><p>68&deg; F</p></td>
<td><p>273.15 + 20 K</p></td>
</tr>
</table>
<p><br><br><br><br><b><font style=\"font-size: 7pt; \">Domestic Water System parameters</b></p>
<table cellspacing=\"2\" cellpadding=\"0\" border=\"0\"><tr>
<td><h4>Name</h4></td>
<td><h4>IP units</h4></td>
<td><h4>SI units</h4></td>
</tr>
<tr>
<td><p>VTan</p></td>
<td><p>792.516 gal</p></td>
<td><p>3 m^3</p></td>
</tr>
<tr>
<td><p>hTan</p></td>
<td><p>9.84252 ft</p></td>
<td><p>3 m</p></td>
</tr>
<tr>
<td><p>dIns</p></td>
<td><p>1.97 in&nbsp;</p></td>
<td><p>0.05 m</p></td>
</tr>
<tr>
<td><p>Q_flow_DWnominal</p></td>
<td><p>2047284.9798 MBH</p></td>
<td><p>600000 W</p></td>
</tr>
<tr>
<td><p>TBoiSetIn</p></td>
<td><p>68&deg; F</p></td>
<td><p>273.15 + 20 K</p></td>
</tr>
</table>
<p><br><br><h4><span style=\"color:#008000\">Results</span></h4></p>
<p><img src=\"modelica://HotelModel/../HotelInfo/Results0.PNG\"/></p>
<p><br><img src=\"modelica://HotelModel/../HotelInfo/Results1.PNG\"/></p>
<p><br><h5>Notes on the results:</h5></p>
<p><br>        &GT; 549.82 hp [410 kW] </p>
<p>        &GT; 556.52 hp [415 kW]</p>
<p>        &GT; 23.78 gpm [1.5 kg/s]</p>
<p>        &GT; 19.02 gpm [1.2 kg/s]</p>
<p><br><h4><span style=\"color:#008000\">Conclusion</span></h4></p>
<p><br>The system showed that during the greatest changes in system inputs, there was about 25&percnt; energy savings using a heat recovery system. </p>
</html>"));
end HotelModel_annual_baseline1;
