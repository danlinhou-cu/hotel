within HotelModel.RainWaterCollectionSystem;
model RainWaterSimpleCollection
  "Simulation for the rain water collection system"
 extends Modelica.Icons.Example;
  replaceable package MediumRainWater =
      Buildings.Media.ConstantPropertyLiquidWater
    "Medium in the condenser water side";
  replaceable package MediumCityWater =
      Buildings.Media.ConstantPropertyLiquidWater
    "Medium in the condenser water side";
  RainWaterCollectionTankSimpleModel
                               rainWaterCollectionTankModel(
    redeclare package MediumRainWater = MediumRainWater,
    redeclare package MediumCityWater = MediumCityWater,
    m_RWflow_nominal=7.89,
    dpSSFil_nominal(displayUnit="kPa") = 68940,
    dpUVFil_nominal(displayUnit="kPa") = 68940,
    dpRW_nominal(displayUnit="kPa") = 1103160,
    m_CitWatflow_nominal=551.58,
    m_valveflow_nominal=2,
    dpValve_nominal(displayUnit="kPa") = 34470,
    SSFilIn=0,
    UVFilIn=0,
    dp_OFnominal(displayUnit="kPa") = 1000,
    m_OFflow_nominal=0.5,
    StoTanVol=1,
    area=159,
    StoSys(onOFF_Control(waiTim=600)),
    ColTanVol=6.06*100*4)
    annotation (Placement(transformation(extent={{-62,-10},{-42,10}})));
  Buildings.Fluid.Sources.FixedBoundary bou1(nPorts=2, redeclare package Medium =
        MediumRainWater)
    annotation (Placement(transformation(extent={{76,-10},{56,10}})));
  Buildings.Fluid.Movers.FlowMachine_m_flow
                                        pumToi(redeclare package Medium =
        MediumRainWater, m_flow_nominal=7.89)
    annotation (Placement(transformation(extent={{0,-10},{20,10}})));
public
  Modelica.Blocks.Logical.Switch switch1
    annotation (Placement(transformation(extent={{20,50},{40,70}})));
  Modelica.Blocks.Sources.BooleanExpression booleanExpression(y=
        rainWaterCollectionTankModel.StoSys.VolOut < 0.1)
    annotation (Placement(transformation(extent={{-66,70},{-46,90}})));
  Modelica.Blocks.Sources.Constant const(k=0)
    annotation (Placement(transformation(extent={{-100,80},{-80,100}})));
public
  Modelica.Blocks.Logical.Switch switch2
    annotation (Placement(transformation(extent={{0,-80},{20,-60}})));
  Buildings.Fluid.Sources.MassFlowSource_h CityWat(
    use_m_flow_in=true,
    nPorts=1,
    redeclare package Medium = MediumRainWater)
    annotation (Placement(transformation(extent={{0,-50},{20,-30}})));
  Modelica.Blocks.Sources.CombiTimeTable makeupWater(
    tableOnFile=true,
    tableName="tab1",
    fileName="C:/Users/Danlin/Documents/Dymola/Hotel/V4-Danlin/to_be_merged/HotelModel/usage2.txt")
    annotation (Placement(transformation(extent={{-98,42},{-78,62}})));
  Modelica.Blocks.Sources.CombiTimeTable rainwater(
    tableOnFile=true,
    tableName="tab1",
    fileName=
        "C:/Users/Danlin/Documents/Dymola/Hotel/V4-Danlin/to_be_merged/HotelModel/rainwater.txt")
    "rain water profile"
    annotation (Placement(transformation(extent={{-96,-42},{-76,-22}})));
  Modelica.Blocks.Continuous.Integrator citWatTot "Total mass of city water"
    annotation (Placement(transformation(extent={{66,-80},{86,-60}})));
  Modelica.Blocks.Continuous.Integrator raiWatTol "Total mass of rain water"
    annotation (Placement(transformation(extent={{-68,-68},{-48,-48}})));
  Modelica.Blocks.Sources.RealExpression rainwatermass(y=
        rainWaterCollectionTankModel.ColTan.MasRWFloRat) "mass flow rate"
    annotation (Placement(transformation(extent={{-98,-68},{-78,-48}})));
  Modelica.Blocks.Sources.CombiTimeTable toilet(
    tableOnFile=true,
    tableName="tab1",
    fileName=
        "C:/Users/Danlin/Documents/Dymola/Hotel/V4-Danlin/to_be_merged/HotelModel/usagetoilet.txt",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic)
    annotation (Placement(transformation(extent={{-98,16},{-78,36}})));
  Modelica.Blocks.Math.Add add
    annotation (Placement(transformation(extent={{-66,30},{-46,50}})));
  Modelica.Blocks.Continuous.Integrator citWatTot1
                                                  "Total mass of city water"
    annotation (Placement(transformation(extent={{-62,14},{-50,24}})));
  Modelica.Blocks.Continuous.Integrator citWatTot2
                                                  "Total mass of city water"
    annotation (Placement(transformation(extent={{-22,24},{-10,34}})));
equation
  connect(rainWaterCollectionTankModel.CooTow, pumToi.port_a)
    annotation (Line(points={{-42,0},{0,0}},color={0,127,255},
      thickness=1));
  connect(pumToi.port_b, bou1.ports[1])
    annotation (Line(points={{20,0},{56,0},{56,2}},
                                             color={0,127,255},
      thickness=1));
  connect(booleanExpression.y, switch1.u2) annotation (Line(points={{-45,80},{
          -46,80},{-46,80},{-44,80},{-10,80},{0,80},{0,60},{18,60}}, color={255,
          0,255}));
  connect(switch1.y, pumToi.m_flow_in) annotation (Line(points={{41,60},{52,60},
          {58,60},{58,32},{9.8,32},{9.8,12}}, color={0,0,127}));
  connect(switch2.u2, switch1.u2) annotation (Line(points={{-2,-70},{-8,-70},{
          -8,80},{0,80},{0,60},{18,60}}, color={255,0,255}));
  connect(CityWat.ports[1], bou1.ports[2]) annotation (Line(
      points={{20,-40},{20,-40},{34,-40},{34,-2},{56,-2}},
      color={0,127,255},
      thickness=1));
  connect(switch2.y, CityWat.m_flow_in) annotation (Line(points={{21,-70},{36,
          -70},{36,-50},{-22,-50},{-22,-32},{0,-32}}, color={0,0,127}));
  connect(const.y, switch1.u1) annotation (Line(points={{-79,90},{-79,90},{12,
          90},{12,68},{18,68}}, color={0,0,127}));
  connect(switch2.u1, switch1.u3) annotation (Line(points={{-2,-62},{-30,-62},{
          -30,52},{18,52}}, color={0,0,127}));
  connect(switch2.u3, switch1.u1) annotation (Line(points={{-2,-78},{-40,-78},{
          -40,90},{12,90},{12,68},{18,68}}, color={0,0,127}));
  connect(rainwater.y[1], rainWaterCollectionTankModel.RaiWatIn1) annotation (
      Line(points={{-75,-32},{-72,-32},{-72,4.2},{-64,4.2}},
                                                           color={0,0,127}));
  connect(switch2.y, citWatTot.u)
    annotation (Line(points={{21,-70},{64,-70}}, color={0,0,127}));
  connect(raiWatTol.u, rainwatermass.y)
    annotation (Line(points={{-70,-58},{-77,-58}}, color={0,0,127}));
  connect(makeupWater.y[1], add.u1) annotation (Line(points={{-77,52},{-74,52},
          {-74,46},{-68,46}}, color={0,0,127}));
  connect(toilet.y[1], add.u2) annotation (Line(points={{-77,26},{-74,26},{-74,
          34},{-68,34}}, color={0,0,127}));
  connect(add.y, switch1.u3) annotation (Line(points={{-45,40},{-30,40},{-30,52},
          {18,52}}, color={0,0,127}));
  connect(add.y, switch2.u1) annotation (Line(points={{-45,40},{-30,40},{-30,
          -62},{-2,-62}}, color={0,0,127}));
  connect(toilet.y[1], citWatTot1.u) annotation (Line(points={{-77,26},{-70,26},
          {-70,19},{-63.2,19}}, color={0,0,127}));
  connect(add.y, citWatTot2.u) annotation (Line(points={{-45,40},{-34,40},{-34,
          29},{-23.2,29}}, color={0,0,127}));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}})),           experiment(StopTime=3.1536e+007));
end RainWaterSimpleCollection;
