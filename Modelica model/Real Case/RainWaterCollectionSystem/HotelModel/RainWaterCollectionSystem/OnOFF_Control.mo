within HotelModel.RainWaterCollectionSystem;
model OnOFF_Control
  parameter Real waiTim "waiting time";
  parameter Real SetPoi "set point";
  Modelica.Blocks.Interfaces.RealInput Mea
    "Connector of measurement input signal" annotation (Placement(
        transformation(extent={{-140,-20},{-100,20}}), iconTransformation(
          extent={{-140,-20},{-100,20}})));
  Modelica.Blocks.Interfaces.RealOutput On
    "Connector of actuator output signal" annotation (Placement(transformation(
          extent={{100,-10},{120,10}}), iconTransformation(extent={{100,-10},{120,
            10}})));
  inner Modelica.StateGraph.StateGraphRoot stateGraphRoot
    annotation (Placement(transformation(extent={{-100,80},{-80,100}})));
  Modelica.StateGraph.InitialStepWithSignal initialStepWithSignal annotation (
      Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=-90,
        origin={0,30})));
  Modelica.StateGraph.StepWithSignal stepWithSignal annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={0,-30})));
  Modelica.StateGraph.Transition transition(
    waitTime=waiTim,
    enableTimer=true,
    condition=Mea < SetPoi*0.5)
                       annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={38,0})));
  Modelica.StateGraph.Transition transition1(
    waitTime=waiTim,
    condition=Mea > SetPoi,
    enableTimer=false) annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=-90,
        origin={-40,0})));
  Modelica.Blocks.Math.MultiSwitch multiSwitch1(expr={0,1}, nu=2)
    annotation (Placement(transformation(extent={{62,-4},{82,4}})));
equation
  connect(transition.inPort, initialStepWithSignal.outPort[1]) annotation (Line(
      points={{38,4},{38,4},{38,10},{0,10},{0,19.5}},
      color={0,0,0},
      pattern=LinePattern.Dash));
  connect(transition.outPort, stepWithSignal.inPort[1]) annotation (Line(
      points={{38,-1.5},{38,-14},{0,-14},{0,-19}},
      color={0,0,0},
      pattern=LinePattern.Dash));
  connect(stepWithSignal.outPort[1], transition1.inPort) annotation (Line(
      points={{0,-40.5},{0,-60},{-40,-60},{-40,-4}},
      color={0,0,0},
      pattern=LinePattern.Dash));
  connect(transition1.outPort, initialStepWithSignal.inPort[1]) annotation (
      Line(
      points={{-40,1.5},{-40,1.5},{-40,60},{0,60},{0,41}},
      color={0,0,0},
      pattern=LinePattern.Dash));
  connect(multiSwitch1.y, On)
    annotation (Line(points={{82.5,0},{82.5,0},{110,0}}, color={0,0,127}));
  connect(initialStepWithSignal.active, multiSwitch1.u[1]) annotation (Line(
      points={{11,30},{56,30},{56,0.6},{62,0.6}},
      color={255,0,255},
      pattern=LinePattern.Dash));
  connect(stepWithSignal.active, multiSwitch1.u[2]) annotation (Line(
      points={{11,-30},{32,-30},{56,-30},{56,-0.6},{62,-0.6}},
      color={255,0,255},
      pattern=LinePattern.Dash));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,100}}),
                   graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,127},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-68,64},{74,-72}},
          lineColor={0,0,127},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textString="C"),       Text(
          extent={{-155,141},{145,101}},
          lineColor={0,0,255},
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={0,127,255},
          textString="%name")}), Diagram(coordinateSystem(preserveAspectRatio=false,
          extent={{-100,-100},{100,100}})));
end OnOFF_Control;
