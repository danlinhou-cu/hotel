within HotelModel.RainWaterCollectionSystem;
model RainWaterCollectionTankSimpleModel
  "Collection Tank System components not including control sequence"

  replaceable package MediumRainWater =
      Buildings.Media.ConstantPropertyLiquidWater
    "Medium in the condenser water side";
  replaceable package MediumCityWater =
      Buildings.Media.ConstantPropertyLiquidWater
    "Medium in the condenser water side";
  parameter Modelica.SIunits.MassFlowRate m_RWflow_nominal
    "Nominal mass flow rate";
  parameter Modelica.SIunits.Pressure dpSSFil_nominal
    "Pressure drop of solid separator filter";
  parameter Modelica.SIunits.Pressure dpUVFil_nominal
    "Pressure drop of UV filter";
  parameter Modelica.SIunits.Pressure dpRW_nominal "Nominal Pressure drop";
parameter Modelica.SIunits.MassFlowRate m_CitWatflow_nominal
    "Nominal mass flow rate";
  parameter Modelica.SIunits.MassFlowRate m_valveflow_nominal
    "Nominal mass flow rate";
  parameter Modelica.SIunits.Pressure dpValve_nominal
    "Nominal pressure drop of fully open valve, used if CvData=Buildings.Fluid.Types.CvTypes.OpPoint";
  parameter Real SSFilIn "Constant output value";
  parameter Real UVFilIn "Constant output value";
 parameter Modelica.SIunits.Volume ColTanVol "Volume";
  parameter Modelica.SIunits.Pressure dp_OFnominal
    "Pressure drop at nominal mass flow rate";
  parameter Modelica.SIunits.MassFlowRate m_OFflow_nominal
    "Nominal mass flow rate";
  parameter Real area "Constant output value";
  parameter Modelica.SIunits.Volume StoTanVol "Volume";
  CollectionSystem.CollectionTankModel ColTan(redeclare package MediumRainWater =
        MediumRainWater, area=area)           annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-70,30})));
  StorageSystem.StorageSystemModel_try
                                   StoSys(
    redeclare package MediumRainWater = MediumRainWater,
    m_RWflow_nominal=m_RWflow_nominal,
    dpRW_nominal=dpRW_nominal,
    dp_OFnominal=dp_OFnominal,
    ColTanVol=ColTanVol,
    m_OFflow_nominal=m_OFflow_nominal)
    annotation (Placement(transformation(extent={{-8,-10},{12,10}})));
  Modelica.Fluid.Interfaces.FluidPort_b CooTow(redeclare package Medium =
        MediumRainWater)
    "Fluid connector b (positive design flow direction is from port_a to port_b)"
    annotation (Placement(transformation(extent={{90,-10},{110,10}})));
  Modelica.Blocks.Interfaces.RealInput RaiWatIn1
    "Connector of Real input signal"
    annotation (Placement(transformation(extent={{-140,22},{-100,62}})));
equation
  connect(ColTan.ports1[1], StoSys.port_a1) annotation (Line(
      points={{-70,20.2},{-70,0},{-8,0}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(CooTow, StoSys.port_b1) annotation (Line(
      points={{100,0},{12,0}},
      color={0,127,255},
      thickness=1));
  connect(StoSys.On1, ColTan.u)
    annotation (Line(points={{-9,3.8},{-66,3.8},{-66,19}}, color={0,0,127}));
  connect(ColTan.RaiWatIn1, RaiWatIn1) annotation (Line(points={{-74,42},{-120,
          42}},                    color={0,0,127}));
  annotation (Diagram(coordinateSystem(extent={{-100,-100},{100,100}},
          preserveAspectRatio=false)),           Icon(coordinateSystem(extent={{-100,
            -100},{100,100}}, preserveAspectRatio=false), graphics={
        Ellipse(
          extent={{-60,-40},{60,-60}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-60,70},{60,-50}},
          lineColor={255,255,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{60,6},{94,-6}},
          fillColor={0,128,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Ellipse(
          extent={{-60,80},{60,60}},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Rectangle(
          extent={{-60,40},{60,-50}},
          fillColor={0,128,255},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Ellipse(
          extent={{-60,-40},{60,-60}},
          lineColor={0,0,0},
          fillColor={0,128,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-60,30},{60,50}},
          lineColor={0,0,0},
          fillColor={0,128,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-60,-24},{60,-50}},
          fillColor={0,128,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Line(
          points={{-60,70},{-60,-50}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{60,70},{60,-50}},
          color={0,0,0},
          smooth=Smooth.None),
        Rectangle(
          extent={{-6,92},{6,70}},
          pattern=LinePattern.None,
          fillColor={0,128,255},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Text(
          extent={{-60,40},{58,-60}},
          pattern=LinePattern.None,
          fillColor={0,128,255},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0},
          textString="RWC")}));
end RainWaterCollectionTankSimpleModel;
