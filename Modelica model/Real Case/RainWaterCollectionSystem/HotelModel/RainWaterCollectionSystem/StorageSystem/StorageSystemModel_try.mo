within HotelModel.RainWaterCollectionSystem.StorageSystem;
model StorageSystemModel_try "Storage System Model "
  replaceable package MediumRainWater =
      Buildings.Media.ConstantPropertyLiquidWater
    "Medium in the condenser water side";
  parameter Modelica.SIunits.MassFlowRate m_RWflow_nominal
    "Nominal mass flow rate";
  parameter Modelica.SIunits.Pressure dpRW_nominal "Nominal Pressure drop";
  parameter Modelica.SIunits.Pressure dp_OFnominal
    "Pressure drop at nominal mass flow rate";
  Modelica.Fluid.Interfaces.FluidPort_b port_b1(redeclare package Medium =
        MediumRainWater)
    "Fluid connector b (positive design flow direction is from port_a to port_b)"
    annotation (Placement(transformation(extent={{90,-10},{110,10}})));
  Modelica.Fluid.Interfaces.FluidPort_a port_a1(redeclare package Medium =
        MediumRainWater)
    "Fluid connector a (positive design flow direction is from port_a to port_b)"
    annotation (Placement(transformation(extent={{-110,-10},{-90,10}})));
  Buildings.Fluid.Sensors.MassFlowRate senMasFloIn(redeclare package Medium =
        MediumRainWater)
    "Mass flow rate of water going into the collection tank"
    annotation (Placement(transformation(extent={{-72,-10},{-52,10}})));
  Buildings.Fluid.Sensors.MassFlowRate senMasFloOut(redeclare package Medium =
        MediumRainWater)
    "Mass flow rate of water going into the collection tank"
    annotation (Placement(transformation(extent={{50,-10},{70,10}})));
  parameter Modelica.SIunits.Volume ColTanVol "Volume";

  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{70,70},{90,90}})));

  parameter Modelica.SIunits.MassFlowRate m_OFflow_nominal
    "Nominal mass flow rate";
  Modelica.Blocks.Interfaces.RealOutput VolOut "Value of Real output"
    annotation (Placement(transformation(extent={{100,50},{120,70}})));
  OutputVolume OutVol(
    redeclare package Medium = MediumRainWater,
    V=ColTanVol,
    use_portsData=false)
    annotation (Placement(transformation(extent={{-10,0},{10,20}})));
  OnOFF_Control onOFF_Control(waiTim=300, SetPoi=ColTanVol*0.01)
    annotation (Placement(transformation(extent={{-60,50},{-40,70}})));
  Modelica.Blocks.Interfaces.RealOutput On1
    "Connector of actuator output signal"
    annotation (Placement(transformation(extent={{-100,28},{-120,48}})));
equation
  connect(OutVol.ports1[1], senMasFloIn.port_b) annotation (Line(
      points={{-3.46667,0.1},{-10,0.1},{-10,0},{-52,0}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(OutVol.ports1[3], senMasFloOut.port_a) annotation (Line(
      points={{3.46667,0.1},{10,0.1},{10,0},{50,0}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(OutVol.VolOut, VolOut) annotation (Line(
      points={{11,10},{20,10},{20,60},{110,60}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash));
  connect(senMasFloIn.port_a, port_a1) annotation (Line(
      points={{-72,0},{-72,0},{-100,0}},
      color={0,127,255},
      thickness=1));
  connect(senMasFloOut.port_b, port_b1) annotation (Line(
      points={{70,0},{100,0}},
      color={0,127,255},
      thickness=1));
  connect(onOFF_Control.Mea, VolOut) annotation (Line(
      points={{-62,60},{-80,60},{-80,96},{20,96},{20,60},{110,60}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(onOFF_Control.On, On1) annotation (Line(points={{-39,60},{0,60},{0,38},
          {-110,38}}, color={0,0,127}));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}})),           Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics={
        Rectangle(
          extent={{-60,60},{60,-80}},
          lineColor={95,95,95},
          fillPattern=FillPattern.Solid,
          fillColor={0,128,255}),
        Ellipse(
          extent={{-60,20},{-20,60}},
          lineColor={95,95,95},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-20,20},{20,60}},
          lineColor={95,95,95},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{20,20},{60,60}},
          lineColor={95,95,95},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-60,60},{60,38}},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Line(
          points={{-60,38},{-60,60},{-36,60},{60,60},{60,36}},
          pattern=LinePattern.None,
          smooth=Smooth.None),
        Text(
          extent={{-100,100},{100,60}},
          pattern=LinePattern.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textString="%name",
          lineColor={0,0,255})}));
end StorageSystemModel_try;
