# -*- coding: utf-8 -*-
"""
Created on Fri Oct 06 15:40:45 2017

@author: Danlin
"""

from buildingspy.io.outputfile import Reader
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
import matplotlib.pyplot as plt
import numpy as np


r=Reader('HotelModel_annual_day.mat','dymola') # Read parameters
(t1,y1)=r.values('load.TDryBul') # Outdoor dry bulb temperature
(t2,y2)=r.values('supCon.THeatPump') # Heatpump’s outlet water temperature (T2)
(t3,y3)=r.values('supCon.TBoiHP') # Heat exchanger (between heatpump and cooling tower) inlet water temperature for the heatpump side (T3)
(t4,y4)=r.values('supCon.TBoiDW') # Hot water tank’s outlet water temperature (T4)
(t5,y5)=r.values('supCon.masFloHotWat') # Mass flow rate of DHW (F4)
(t6,y6)=r.values('supCon.y')
#(t7,y7)=r.values('heatPump.HexVal.Hex.hex.Q1_flow')
t1d=[]
y1d=[]
y2d=[]
y3d=[]
y4d=[]
y5d=[]
y6d=[]
#y7d=[]

for i in range(len(y6)):
    if t6[i]==2651805.5: 
        y6[i]=6

for i in range(len(y1)):
    if t1[i]>30*24*3600 and t1[i]<31*24*3600:
        t1d.append(t1[i])
        y1d.append(y1[i]-273.15)
        y2d.append(y2[i]-273.15)
        y3d.append(y3[i]-273.15)
        y4d.append(y4[i]-273.15)
        y5d.append(y5[i])
        y6d.append(y6[i])
        #y7d.append(y7[i])

host = host_subplot(111, axes_class=AA.Axes)
plt.subplots_adjust(right=0.75)

par1 = host.twinx()
par2 = host.twinx()
#par3 = host.twinx()

offset = 60
new_fixed_axis = par2.get_grid_helper().new_fixed_axis
par2.axis["right"] = new_fixed_axis(loc="right", axes=par2,
                                        offset=(offset, 0))

par2.axis["right"].toggle(all=True)

host.set_xlim(2595600, 2675623.8)


host.set_xlabel("Time [h]")
host.set_ylabel("State")
par1.set_ylabel("Temperature [$^\circ$C]")
par2.set_ylabel("Mass flow [kg/s]")

p1,=host.plot(t1d,y6d,'r-',label='State',lw=5.0)
p2,=par1.plot(t1d,y1d,'b--',label='Tdb',lw=0.75)
p2,=par1.plot(t1d,y2d,'c--',label='T2',lw=0.75)
p2,=par1.plot(t1d,y3d,'y--',label='T3',lw=0.75)
p2,=par1.plot(t1d,y4d,'k--',label='T4',lw=0.75)
p3,=par2.plot(t1d,y5d,'m--',label='F4',lw=0.75)
#p4,=par3.plot(t1d,y7d,'g--',label='hexEng')

host.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
           ncol=6, mode="expand", borderaxespad=0.)

host.axis["left"].label.set_color('red')
par1.axis['right'].label.set_color(p2.get_color())
par2.axis['right'].label.set_color('m')
#plt.xticks([2595600, 2599200, 2602800, 2606400, 2610000, 72613600, 2617200, 2620800, 2624400, 2628000, 2631600, 2635200, 2638800, 2642400, 2646000, 2649600, 2653200, 2656800, 2660400, 2664000, 2667600, 2671200, 2674800, 2678400],
           #['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'])
new_ticks=np.linspace(2595600,2675623.8,7)
plt.xticks(new_ticks,['0','4','8','12','16','20','24'])
Fig = plt.gcf()
Fig.set_size_inches(14, 4)
plt.savefig('specialDay.png', format='png', dpi=1000)
plt.draw()
plt.show()
'''
from buildingspy.io.outputfile import Reader
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
import matplotlib.pyplot as plt


r=Reader('HotelModel_annual_day.mat','dymola') # Read parameters
(t1,y1)=r.values('load.TDryBul') # Outdoor dry bulb temperature
(t2,y2)=r.values('supCon.THeatPump') # Heatpump’s outlet water temperature (T2)
(t3,y3)=r.values('supCon.TBoiHP') # Heat exchanger (between heatpump and cooling tower) inlet water temperature for the heatpump side (T3)
(t4,y4)=r.values('supCon.TBoiDW') # Hot water tank’s outlet water temperature (T4)
(t5,y5)=r.values('supCon.masFloHotWat') # Mass flow rate of DHW (F4)
(t6,y6)=r.values('supCon.y')
t1d=[]
y1d=[]
y2d=[]
y3d=[]
y4d=[]
y5d=[]
y6d=[]
t1d2=[]
y2d1=[]

for i in range(len(y6)):
    if t6[i]==2651805.5: 
        y6[i]=6
       # print y6[i]

for i in range(len(y1)):
    if t1[i]>30*24*3600 and t1[i]<31*24*3600:
        t1d.append(t1[i])
        y1d.append(y1[i]-273.15)
        y2d.append(y2[i]-273.15)
        y3d.append(y3[i]-273.15)
        y4d.append(y4[i]-273.15)
        y5d.append(y5[i])
        y6d.append(y6[i])
        if y6[i+1]!=y6[i]:
            print('state1 is equal to: ',y6[i])
            print('state2 is equal to: ',y6[i+1])
            print('event time is: ',t1[i])
            print ('T2 is: ', y2[i]-273.15)
            print ('T3 is: ', y3[i]-273.15)
            print ('T4 is: ', y4[i]-273.15)
            print ('F4 is: ', y5[i])
          
for i in range(len(y1)):
    if t1[i]>30*24*3600 and t1[i]<=2616937:
        t1d1.append(t1[i])
        y1d1.append(y1[i]-273.15)
        y2d1.append(y2[i]-273.15)
        y3d1.append(y3[i]-273.15)
        y4d1.append(y4[i]-273.15)
        y5d1.append(y5[i])
        y6d1.append(y6[i])
    if t1[i]>2616937 and t1[i]<=:
        
    

host = host_subplot(111, axes_class=AA.Axes)
plt.subplots_adjust(right=0.75)

par1 = host.twinx()
par2 = host.twinx()

offset = 60
new_fixed_axis = par2.get_grid_helper().new_fixed_axis
par2.axis["right"] = new_fixed_axis(loc="right", axes=par2,
                                        offset=(offset, 0))

par2.axis["right"].toggle(all=True)

host.set_xlabel("Time [h]")
host.set_ylabel("State")
par1.set_ylabel("Temperature [$^\circ$C]")
par2.set_ylabel("Mass flow [kg/s]")

p1,=host.plot(t1d,y6d,'r-',label='State',lw=4)
p2,=par1.plot(t1d,y1d,'c--',label='Tdb',lw=1)
p2,=par1.plot(t1d,y2d,'m--',label='T2',lw=1)
p2,=par1.plot(t1d,y3d,'y--',label='T3',lw=1)
p2,=par1.plot(t1d,y4d,'b--',label='T4',lw=1)
p2,=par1.plot(t1d2,y2d1,'m-',label='T2',lw=3)
        
p3,=par2.plot(t1d,y5d,'g--',label='F4',lw=1)


host.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
           ncol=6, mode="expand", borderaxespad=0.)

host.axis["left"].label.set_color('red')
#par1.axis['right'].label.set_color(p2.get_color())
par2.axis['right'].label.set_color('g')
Fig = plt.gcf()
Fig.set_size_inches(14, 4)


plt.savefig('specialDay.png', format='png', dpi=1000)
plt.draw()
plt.show()
'''