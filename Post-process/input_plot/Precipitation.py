# -*- coding: utf-8 -*-
"""
Created on Sat Sep 30 17:33:07 2017

@author: Danlin
"""


import matplotlib.pyplot as plt
import numpy as np
import math
import pandas as pd
table=table5=pd.read_csv('Rainwater.csv',header=None, names=['Rainwater'])

Rainwater=[]
high_precipitation=[]

for i in range(0, len(table)):
    Rainwater.append(table['Rainwater'].iloc[i])
    
index=Rainwater.index(max(Rainwater)) # Find the hour appeared highest precipitation
day=int(math.floor(index/24)) # Find days before the appearence of highest precipitation
#print (len(Rainwater))
for j in range(day*24,day*24+24):
    high_precipitation.append(Rainwater[j])

#x1=[0.5*720,1.5*720,2.5*720,3.5*720,4.5*720,5.5*720,6.5*720,7.5*720,8.5*720,9.5*720,10.5*720,11.5*720]
x1=[0,744,1416,2160,2880,3624,4344,5088,5832,6552,7296,8016]
xlab=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']

# Plot figure  
fig=plt.figure()
left,bottom,width,height=0.1,0.1,0.8,0.8
ax1=fig.add_axes([left,bottom,width,height])
ax1.plot(Rainwater)
ax1.set_xlabel('Time [h]')
ax1.set_ylabel('Precipatation Depth [mm](2016)')
ax1.set_xticks(x1)
ax1.set_xticklabels(xlab)
ax1.set_xlim(0,8760)

# Plot figure in figure
left,bottom,width,height=0.18,0.56,0.25,0.25 # Decide the position of the small figure
ax2=fig.add_axes([left,bottom,width,height])
ax2.plot(high_precipitation,'r')
#ax2.set_xlim(0,24,1)
new_ticks=np.linspace(0,24,4)
print(new_ticks)
ax2.set_xticks(new_ticks)
#ax2.set_xlabel('Time [h]',fontsize=8)
ax2.set_title('Precipatation [08/01/2016]',fontsize=10)

# Add an arrow
ax1.annotate("",xy=(3850,38),xycoords='data',xytext=(4850,38),textcoords='data',arrowprops=dict(arrowstyle="->",connectionstyle="arc3"),)
