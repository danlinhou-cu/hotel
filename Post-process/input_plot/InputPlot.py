# -*- coding: utf-8 -*-
"""
Created on Sun Sep 24 10:39:21 2017

@author: Danlin
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
#import xlrd

table1=pd.read_csv('outdoorT.csv',header=None, names=['outdoorT'])
#fig, (ax1) = plt.subplots(nrows=1, sharex=False)

table2=pd.read_csv('load.csv',header=None, names=['load'])
#fig, (ax2) = plt.subplots(nrows=1, sharex=False)

table3=pd.read_csv('DHW.csv',header=None, names=['DHW'])
#fig, (ax3) = plt.subplots(nrows=1, sharex=False)

table4=pd.read_csv('KIT.csv',header=None, names=['KIT'])
#fig, (ax3) = plt.subplots(nrows=1, sharex=False)

table5=pd.read_csv('Rainwater.csv',header=None, names=['Rainwater'])
#fig, (ax5) = plt.subplots(nrows=1, sharex=False)

x=np.arange(1440)
num=len(table1)/1440

print num

for i in range(num):
     load=[]
     outdoorT=[]
     for j in range(1440*i,1440*i+1440):
         outdoorT.append(table1['outdoorT'].iloc[j])
         load.append(table2['load'].iloc[j]/1000)
    
     ax1=plt.subplot(4,1,1)
     ax1.scatter(x,outdoorT,s=0.01,marker='o')
     ax2=plt.subplot(4,1,2)
     ax2.scatter(x,load,s=0.01,marker='o')

DHW=[]
KIT=[]
for k in range(0,24):
    DHW.append(table3['DHW'].iloc[k])
    KIT.append(table4['KIT'].iloc[k])
t=range(0,24)

Rainwater=[]
for a in range(0,8760):
    Rainwater.append(table5['Rainwater'].iloc[a])
    

x1=[0,6*60,12*60,18*60,23*60]
xlab=['0:00','6:00','12:00','18:00','23:00']
ax1.set_xticks(x1)
ax1.set_xticklabels(xlab)
ax1.set_xlim(0,1440)	 
ax1.set_ylabel('Outdoor Temperature [$^\circ$C] (daily)',fontsize=10)
ax1.set_xlabel('Time [h]',fontsize=10)
#Fig = plt.gcf()
#ax1.set_size_inches(10, 4)
	

x2=[0,6*60,12*60,18*60,23*60]
xlab=['0:00','6:00','12:00','18:00','23:00']
ax2.set_xticks(x2)
ax2.set_xticklabels(xlab)
ax2.set_xlim(0,1440)	  
#ax2 = plt.subplot(2,1,2)
#ax2.plot(t1d,y1d,color='black')
ax2.set_ylabel('Cooling / Heating Load [kW] (daily)',fontsize=10)
ax2.set_xlabel('Time [h]',fontsize=10)

ax3=plt.subplot(4,1,3)
ax3.plot(t,DHW,label="Guestroom")
ax3.plot(t,KIT,label='Kitchen')
plt.legend()
x3=[0,6,12,18,23]
xlab=['0:00','6:00','12:00','18:00','23:00']
ax3.set_xticks(x3)
ax3.set_xticklabels(xlab)
ax3.set_xlim(0,24)	 
ax3.set_ylabel('Hot Water Demand [kg/h] (daily)',fontsize=10)
ax3.set_xlabel('Time [h]',fontsize=10)

ax5=plt.subplot(4,1,4)
ax5.plot(Rainwater)    
x5=[0.5*720,1.5*720,2.5*720,3.5*720,4.5*720,5.5*720,6.5*720,7.5*720,8.5*720,9.5*720,10.5*720,11.5*720]
xlab=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
ax5.set_xticks(x5)
ax5.set_xticklabels(xlab)
ax5.set_xlim(0,8760)	  
ax5.set_ylabel('Precipatation Depth [mm]',fontsize=10)
ax5.set_xlabel('Time [mo.]',fontsize=10)
Fig = plt.gcf()
Fig.set_size_inches(10, 18)
#plt.savefig('power2.png',bbox_inches='tight')        
plt.show()		