# -*- coding: utf-8 -*-
"""
Created on Mon Oct 16 01:19:57 2017

@author: Danlin
"""

import numpy as np
import matplotlib.pyplot as plt

t1=np.arange(24)
y1=[0.077778, 0.054444, 0.035, 0.031111, 0.054444, 0.101111, 0.186667, 0.225556, 0.21, 0.194444, 0.147778, 0.124444, 0.124444, 0.124444, 0.116667, 0.116667, 0.14, 0.14, 0.085556, 0.101111, 0.101111, 0.124444, 0.116667, 0.093333]

fig=plt.figure()
plt.plot(t1,y1)
new_ticks=np.linspace(0,23,24)
plt.xticks(new_ticks)
plt.xlabel('Time [h]')
plt.ylabel('Toilet flush water profile [kg/s]')
plt.show()
