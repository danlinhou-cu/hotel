# -*- coding: utf-8 -*-
"""
Created on Sun Sep 24 15:58:38 2017

@author: Danlin
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

table3=pd.read_csv('DHW.csv',header=None, names=['DHW'])
fig, (ax3) = plt.subplots(nrows=1, sharex=False)

table4=pd.read_csv('KIT.csv',header=None, names=['KIT'])
#fig, (ax3) = plt.subplots(nrows=1, sharex=False)

DHW=[]
KIT=[]
for k in range(0,24):
    DHW.append(table3['DHW'].iloc[k])
    KIT.append(table4['KIT'].iloc[k])
t=range(0,24)
     
ax3.plot(t,DHW,label="Guestroom")
ax3.plot(t,KIT,label='Kitchen')
plt.legend()
x3=[0,6,12,18,23]
xlab=['0:00','6:00','12:00','18:00','23:00']
ax3.set_xticks(x3)
ax3.set_xticklabels(xlab)
ax3.set_xlim(0,24)	 
ax3.set_ylabel('Hot Water Demand [kg/h] (daily)',fontsize=10)
ax3.set_xlabel('Time [h]',fontsize=10)