# -*- coding: utf-8 -*-
"""
Created on Sat Oct 28 09:11:04 2017

@author: Danlin
"""

'''
This code is used to plot the test of control logic
The data is from the special day model
This model is simulated annually and only the data from 30th daya is used
'''

from buildingspy.io.outputfile import Reader
import matplotlib.pyplot as plt
import numpy as np

r=Reader('HotelModel_annual_specialday_ori365.mat','dymola') # Read parameters

# design condition

(t8,y8)=r.values('load.Loa') # Load
(t9,y9)=r.values('domesticWaterControls.m_flow_in_kit') # mass flow rate to kit
(t10,y10)=r.values('domesticWaterControls.m_flow_in_dom') # mass flow rate to guestroom
(t11,y11)=r.values('domesticWaterControls.domesticHotWaterSystem.boi.QWat_flow') # Heat demand of the boiler in DHW system
(t12,y12)=r.values('heatPump.HeaPumBoi.boi.boi.QWat_flow') # Heat demand of the boiler in Heat pump system
(t13,y13)=r.values('coolingTowerSystem.coolingTower.yorkCalc.Q_flow') # Heat rejection of cooling tower     nagetive values

t8dh=[]
t8dc=[]
y8dh=[]  # heating Load
y8dc=[]  # cooling Load
y9dh=[]  # demand to kit
y9dc=[]  # demand to kit
y10dh=[] # demand to guestroom
y10dc=[] # demand to guestroom
y11d=[] # 
y12d=[]
y13d=[]
y14dh=[] # Heat demand of DHW
y14dc=[] # Heat demand of DHW
y15dh=[] # heating load + DHW heating demand


sta_ideh=[]
sta_idec=[]

for i in range(len(y8)):
    if t8[i]>=30*24*3600 and t8[i]<31*24*3600:
        if y8[i]<0:
            t8dh.append(t8[i])
            y8dh.append(-y8[i]/1000)  # heating load
            y14dh.append((4200*y9[i]*(60-(300-273.15))+4200*y10[i]*(43.3-(300-273.15)))/1000) # DHW heat demand
            
        else:
            t8dc.append(t8[i]) 
            y8dc.append(y8[i]/1000) # cooling load
            y14dc.append((4200*y9[i]*(60-(300-273.15))+4200*y10[i]*(43.3-(300-273.15)))/1000) # DHW heat demand
            
for i in range (len(y8dh)):
    y15dh.append(y8dh[i]+y14dh[i]) # heating load + DHW heating demand
        
capacity_b1=max(y14dh+y14dc) # capacity of the boiler in DHW system
capacity_b2=max(y8dh) # capacity of the boiler in HP system      

for i in range(len(y8dh)):
    if capacity_b1 >= y15dh[i]:
            sta_ideh.append(3)
    else:
        if y15dh[i]<(capacity_b1+capacity_b2):
            sta_ideh.append(2)
        else:
            sta_ideh.append(1) 

for i in range(len(y8dc)):
    if y8dc[i]<=y14dc[i]:
        sta_idec.append(5)
    else:
        sta_idec.append(6)
'''       
ax1=plt.subplot(2,1,1)
ax1.scatter(t8dh,sta_ideh,label='Heating State',color='red',s=3)
ax1.scatter(t8dc,sta_idec,label='Cooling State',color='green',s=3)
ax1.plot([max(t8dh),max(t8dh)],[0,8],linestyle='--',color='black',lw=1)
plt.legend()
plt.ylim(0,8)
plt.setp(ax1.get_xticklabels(), visible=False)

ax2=plt.subplot(2,1,2)
ax2.plot(t8dh,y14dh,label='DHW Heating Demand',linestyle='--',color='r')
ax2.plot(t8dh,y15dh,label='Heating Load + DHW Heating Demand',linestyle='-')
ax2.plot([min(t8dh),max(t8dh)],[408.352,408.352],label='Capacity of Boiler 1',linestyle='--')
ax2.plot(t8dc,y8dc,label='Cooling Load')
ax2.plot(t8dc,y14dc,linestyle='--',color='r')
ax2.plot([max(t8dh),max(t8dh)],[0,max(y15dh)],linestyle='--',color='black',lw=1)
plt.legend(bbox_to_anchor=(1.05,1),loc=2,borderaxespad=0.)

Fig = plt.gcf()
Fig.set_size_inches(10, 6)
plt.savefig('IdealStateSpecialDay.png', format='png', dpi=1000)
plt.show()
'''

# Actual condition
(t2,y2)=r.values('supCon.THeatPump') # Heatpump’s outlet water temperature (T2)
(t3,y3)=r.values('supCon.TBoiHP') # Heat exchanger (between heatpump and cooling tower) inlet water temperature for the heatpump side (T3)
(t4,y4)=r.values('supCon.TBoiDW') # Hot water tank’s outlet water temperature (T4)
(t5,y5)=r.values('supCon.masFloHotWat') # Mass flow rate of DHW (F4)
(t6,y6)=r.values('supCon.y')

t2d=[]
y2d=[]
y3d=[]
y4d=[]
y5d=[]
y6d=[]
n_y6=[]
# Change the start point of state
for i in range(len(t2)):
    if t2[i]>=30*24*3600 and t2[i]<31*24*3600:
        t2d.append(t2[i])
        y2d.append(y2[i]-273.15)
        y3d.append(y3[i]-273.15)
        y4d.append(y4[i]-273.15)
        y5d.append(y5[i])
        y6d.append(y6[i])
        
for i in range(len(t2d)):
    if t2d[i]<=30*24*3600+600:
        #n_y6.append(3)
        n_y6.append(y6d[i])
    else:
        n_y6.append(y6d[i])
        

y7=[]

for i in range (len(t2d)):
    y7.append(y2d[i]-y3d[i])
'''
ax1=plt.subplot(6,1,1)
ax1.scatter(t2d,n_y6,label='Actual State',color='red',s=3)
plt.legend()
plt.ylim(0,8)
plt.setp(ax1.get_xticklabels(), visible=False)

ax2=plt.subplot(6,1,2)
ax2.plot(t2d,y2d,label='T2')
ax2.plot([min(t2d),max(t2d)],[16.11+1.12,16.11+1.12],label='S2-S3',linestyle='--',lw=1)
ax2.plot([min(t2d),max(t2d)],[16.11-1.12,16.11-1.12],label='S3-S2',linestyle='--',lw=1)
ax2.plot([min(t2d),max(t2d)],[18.33+1.12,18.33+1.12],label='S3-S4',linestyle='--',lw=1)
ax2.plot([min(t2d),max(t2d)],[18.33-1.12,18.33-1.12],label='S4-S3',linestyle='--',lw=1)
ax2.plot([min(t2d),max(t2d)],[25+1.12,25+1.12],label='S4-S5',linestyle='--',lw=1)
ax2.plot([min(t2d),max(t2d)],[25-1.12,25-1.12],label='S5-S4',linestyle='--',lw=1)
plt.legend()
#plt.setp(ax2.get_xticklabels(), visible=False)
new_ticks=np.linspace(min(t2d),max(t2d),7)
plt.xticks(new_ticks,['0:00','4:00','8:00','12:00','16:00','20:00','24:00'])

ax3=plt.subplot(6,1,3)
ax3.plot(t2d,y3d,label='T3')
ax3.plot([min(t2d),max(t2d)],[28.88+1.12,28.88+1.12],label='S5-S6',linestyle='--',lw=1)
ax3.plot([min(t2d),max(t2d)],[28.88-1.12,28.88-1.12],label='S6-S5',linestyle='--',lw=1)
plt.legend()
plt.setp(ax3.get_xticklabels(), visible=False)

ax4=plt.subplot(6,1,4)
ax4.plot(t2d,y4d,label='T4')
ax4.plot([min(t2d),max(t2d)],[60,60],label='S1-S2',linestyle='--',lw=1)
ax4.plot([min(t2d),max(t2d)],[60-1.12,60-1.12],label='S3-S2 & S2-S1',linestyle='--',lw=1)
plt.legend()
#plt.setp(ax4.get_xticklabels(), visible=False)
new_ticks=np.linspace(min(t2d),max(t2d),7)
plt.xticks(new_ticks,['0:00','4:00','8:00','12:00','16:00','20:00','24:00'])

ax5=plt.subplot(6,1,5)
ax5.plot(t2d,y7,label='T2-T3')
ax5.plot([min(t2d),max(t2d)],[0.56,0.56],label='S6-S7',linestyle='--',lw=1)
plt.legend()
plt.setp(ax5.get_xticklabels(), visible=False)

ax6=plt.subplot(6,1,6)
ax6.plot(t2d,y5d,label='F4')
ax6.plot([min(t2d),max(t2d)],[0.25,0.25],label='S7-S6',linestyle='--',lw=1)
plt.legend()
#plt.setp(ax2.get_xticklabels(), visible=False)

Fig = plt.gcf()
Fig.set_size_inches(10, 20)
plt.savefig('ActualStateSpecialDay.png', format='png', dpi=1000)
plt.show()

plt.scatter(t8dh,sta_ideh,color='green',marker='x',label='Design State')
plt.scatter(t8dc,sta_idec,color='green',marker='x')
plt.plot(t2d,n_y6,label='Actual State',color='red')
plt.grid(color='0.75', linestyle='-.', linewidth=0.5)
plt.legend(loc=4)
plt.ylabel('State')
new_ticks=np.linspace(min(t2d),max(t2d),7)
plt.xticks(new_ticks,['0:00','4:00','8:00','12:00','16:00','20:00','24:00'])
Fig = plt.gcf()
Fig.set_size_inches(10, 4)
plt.savefig('Comparasion.png', format='png', dpi=1000)
plt.show()


plt.plot(t2d,y2d, label='T2')
plt.plot(t2d,y4d, label='T4')
plt.ylabel('Temperature [$^\circ$C]')
new_ticks=np.linspace(min(t2d),max(t2d),7)
plt.xticks(new_ticks,['0:00','4:00','8:00','12:00','16:00','20:00','24:00'])
plt.legend()
Fig = plt.gcf()
Fig.set_size_inches(10, 3)
plt.show()
'''
ax1=plt.subplot(3,1,1)
ax1.scatter(t8dh,sta_ideh,color='green',marker='x',label='Design State')
ax1.scatter(t8dc,sta_idec,color='green',marker='x')
ax1.plot(t2d,n_y6,label='Actual State',color='red')
plt.grid(color='0.75', linestyle='-.', linewidth=0.5)
plt.legend(loc=4)
plt.ylabel('State')
plt.setp(ax1.get_xticklabels(), visible=False)

ax2=plt.subplot(3,1,2)
ax2.plot(t2d,y2d,label='T2')
ax2.plot([min(t2d),max(t2d)],[16.11+1.12,16.11+1.12],label='S2-S3',linestyle='--',lw=1)
ax2.plot([min(t2d),max(t2d)],[16.11-1.12,16.11-1.12],label='S3-S2',linestyle='--',lw=1)
ax2.plot([min(t2d),max(t2d)],[18.33+1.12,18.33+1.12],label='S3-S4',linestyle='--',lw=1)
ax2.plot([min(t2d),max(t2d)],[18.33-1.12,18.33-1.12],label='S4-S3',linestyle='--',lw=1)
ax2.plot([min(t2d),max(t2d)],[25+1.12,25+1.12],label='S4-S5',linestyle='--',lw=1)
ax2.plot([min(t2d),max(t2d)],[25-1.12,25-1.12],label='S5-S4',linestyle='--',lw=1)
plt.legend(loc=4)
plt.ylabel('Temperature [$^\circ$C]')
plt.setp(ax2.get_xticklabels(), visible=False)

ax3=plt.subplot(3,1,3)
ax3.plot(t2d,y4d,label='T4')
ax3.plot([min(t2d),max(t2d)],[60,60],label='S1-S2',linestyle='--',lw=1)
ax3.plot([min(t2d),max(t2d)],[60-1.12,60-1.12],label='S3-S2 & S2-S1',linestyle='--',lw=1)
plt.legend(loc=4)
plt.ylabel('Temperature [$^\circ$C]')
#plt.setp(ax4.get_xticklabels(), visible=False)
new_ticks=np.linspace(min(t2d),max(t2d),7)
plt.xticks(new_ticks,['0:00','4:00','8:00','12:00','16:00','20:00','24:00'])
Fig = plt.gcf()
Fig.set_size_inches(10, 15)
plt.show()
