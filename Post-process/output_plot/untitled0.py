# -*- coding: utf-8 -*-
"""
Created on Mon Oct 16 12:55:36 2017

@author: Danlin
"""

from buildingspy.io.outputfile import Reader
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

r=Reader('HotelModel_annual.mat','dymola')
r1=Reader('HotelModel_annual_baseline1.mat','dymola')

(t1,y1)=r.values('sampler1.y') # Heat rejection of cooling tower
(t2,y2)=r.values('sampler2.y') # Heat recovery in advanced system
(t3,y3)=r1.values('sampler1.y') # Heat consumption of DHW systme
(t4,y4)=r.values('CT_rej_Tot.y') # Total Heat rejection of cooling tower
(t5,y5)=r.values('HeaRecTot.y') # Total heat recovery in advanced system
(t6,y6)=r1.values('HeaConDHWTot.y') # Total Heat consumption of DHW systme

t1s=[]
t2s=[]
t3s=[]
t4s=[]
t5s=[]
t6s=[]
y1s=[]
y2s=[]
y3s=[]
y4s=[]
y5s=[]
y6s=[]
y7s=[] # ideal heat recovery
t7s=[] # time according to y7d
y1d=[]

# Generate ideal heat recovery. ideal heat recovery is equal to minumum of heat rejection of cooling tower and heat consumption of DHW system
for i in range(len(y1)):
    t1s.append(t1[i])
    y1s.append(y1[i])
    y3s.append(y3[i])
    if i%2==0:
        if y1s[i]<y3s[i]:
            t7s.append(t1s[i])
            y7s.append(y1s[i])
        else:
            t7s.append(t1s[i])
            y7s.append(y3s[i])
            
# Integrate everyday's data
for i in range(365):
    for j in range(len(t1)):
        if t1[j]>(86400*i) and t1[j]<(86400*(i+1)):
            y1d.append(r.integral(sampler1.y[j])
