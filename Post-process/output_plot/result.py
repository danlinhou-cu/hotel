# -*- coding: utf-8 -*-
"""
Created on Sun Sep 24 18:26:16 2017

@author: Danlin
"""

from buildingspy.io.outputfile import Reader
import matplotlib.pyplot as plt
from pylab import *
import numpy as np
import matplotlib.dates as mdates
r=Reader('HotelModel_annual.mat','dymola')
(t1,y1)=r.values('Load.y[1]')
(t2,y2)=r.values('supCon.y')
(t3,y3)=r.values('CT_rej.y')
(t4,y4)=r.values('HexVal_2.y')

t1d=[]
t2d=[]
t3d=[]
t4d=[]
y1d=[]
y2d=[]
y3d=[]
y4d=[]

for i in range(len(y1)):
    t1d.append(t1[i])
    y1d.append(y1[i])
    
for j in range(len(y2)):
    t2d.append(t2[j])
    y2d.append(y2[j])
    
for k in range(len(y3)):
    t3d.append(t3[k])
    y3d.append(y3[k])
    
for z in range(len(y4)):
    t4d.append(t1[4])
    y4d.append(y1[4])
    

ax1 = plt.subplot(1,1,1)
ax1.scatter(t4d,y4d,color='red',s=10,marker='o')
#ax1.bar(t3d,y3d,color='green')


'''
ax2=ax1.twinx()
ax2.scatter(t2,y2,color='blue',s=0.1,marker='o')

fig=plt.figure()
host=fig.add_subplot(111)
par=host.twinx()

host.set_ylabel('Heat [w]')
par.set_ylabel('State')

line1,=host.scatter(t1d,y1d,color='red',s=0.01,marker='o')
#line2,=host.bar(t3d,y3d,color='green')
#line3,=host.bar(t4d,y4d,bottom=y3d,color='yellow')
line4, = par.scatter(t2d,y2d,color='blue',s=0.1,marker='o')
host.legend([line1,line2,line3,line4],['Load','Heat Rejection of Cooling Tower','Heat Utilization of DHW System','State'])

plt.bar(t3d,y3d,color='green')
plt.bar(t4d,y4d,bottom=y3d,color='yellow')

'''


Fig = plt.gcf()
Fig.set_size_inches(20, 6)

#plt.savefig('State frequency.png', format='svg', dpi=1000)

plt.show()
