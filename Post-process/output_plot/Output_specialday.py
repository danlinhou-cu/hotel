from buildingspy.io.outputfile import Reader
import matplotlib.pyplot as plt
from pylab import *
import numpy as np

rcParams['legend.loc'] = 'best'
r=Reader('HotelModel_annual.mat','dymola')
(t1,y1)=r.values('weaBus.TDryBul')
(t2,y2)=r.values('Load.y[1]')
(t3,y3)=r.values('supCon.y')
(t4,y4)=r.values('DHWBoi.y')

f, ax = plt.subplots(nrows=4,ncols=1,sharex=True,figsize=(8,10))

y1=y1-273.15
y2=y2/1000
t1d=[]
t2d=[]
t3d=[]
t4d=[]


y1d=[]
y2d=[]
y3d=[]
y4d=[]

a=186

for i in range(len(y1)):
   if t1[i]>=86400*a and t1[i]<86400*(a+1):
    t1d.append(t1[i])
    y1d.append(y1[i])
    
for i in range(len(y2)):
   if t2[i]>=86400*a and t2[i]<86400*(a+1):
    t2d.append(t2[i])
    y2d.append(y2[i])
    
for i in range(len(y3)):
   if t3[i]>=86400*a and t3[i]<86400*(a+1):    
    t3d.append(t3[i])
    y3d.append(y3[i])
    
for i in range(len(y4)):
   if t4[i]>=86400*a and t4[i]<86400*(a+1):      
    t4d.append(t4[i])
    y4d.append(y4[i])
'''	
for i in range(len(y2)):
   if t2[i]>3715200 and t2[i]<3801600:
    t2d.append(t2[i])
    y2d.append(y2[i])	
'''



xlabind=[]
xlab=[]
x=(t1d[len(t1d)-1]-t1d[0])/3600
for i in range(int(round(x+1))):
   xlabind.append(t1d[0]+3600*i)
   xlab.append(str(i))
print xlabind
print xlab

ax1 = plt.subplot(4,1,1)
ax1.plot(t1d,y1d,color='black')
ax1.set_ylabel('Outdoor Temperature [$^\circ$C]')
#ax1.set_xlabel('Time [h]')
#ylim(0,30)
ax1.legend(fontsize='10')
xticks(xlabind,xlab,fontsize='10')


ax2 = plt.subplot(4,1,2)
ax2.plot(t2d,y2d,color='black')
ax2.set_ylabel('Cooling Load [kW]')
#ax2.set_xlabel('Time [h]')
#ylim(0,30)
ax2.legend(fontsize='10')
xticks(xlabind,xlab,fontsize='10')

ax3 = plt.subplot(4,1,3)
ax3.plot(t3d,y3d,color='black')
ax3.set_ylabel('State')
#ax3.set_xlabel('Time [h]')
#ylim(0,30)
ax3.legend(fontsize='10')
xticks(xlabind,xlab,fontsize='10')

ax4 = plt.subplot(4,1,4)
ax4.plot(t4d,y4d,color='black')
ax4.set_ylabel('Gas [m3/s]')
ax4.set_xlabel('Special Day3 [h]')
#ylim(0,30)
ax4.legend(fontsize='10')
xticks(xlabind,xlab,fontsize='10')



#plt.savefig('power2.png',bbox_inches='tight')

print 'the optimization is completed'
