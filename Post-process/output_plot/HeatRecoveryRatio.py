# -*- coding: utf-8 -*-
"""
Created on Wed Oct 11 10:02:35 2017

@author: Danlin
"""
'''
from buildingspy.io.outputfile import Reader
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

r=Reader('HotelModel_annual.mat','dymola')
r1=Reader('HotelModel_annual_baseline1.mat','dymola')

(t1,y1)=r.values('sampler1.y') # Heat rejection of cooling tower
(t2,y2)=r.values('sampler2.y') # Heat recovery in advanced system
(t3,y3)=r1.values('sampler1.y') # Heat consumption of DHW systme without heat recovery
(t4,y4)=r.values('CT_rej_Tot.y') # Total Heat rejection of cooling tower
(t5,y5)=r.values('HeaRecTot.y') # Total heat recovery in advanced system
(t6,y6)=r1.values('HeaConDHWTot.y') # Total Heat consumption of DHW systme

t1s=[]
t2s=[]
t3s=[]
t4s=[]
t5s=[]
t6s=[]
y1s=[]
y2s=[]
y3s=[]
y4s=[]
y5s=[]
y6s=[]
y7s=[] # ideal heat recovery
t7s=[] # time according to y7d
y1d=[]
t1d=[]
y2d=[]
y3d=[]
y1d1=[]
t1d1=[]
y2d1=[]
y3d1=[]
y1d2=[]
y2d2=[]
y3d2=[]
y4d1=[]
y4d2=[]


# Generate ideal heat recovery. ideal heat recovery is equal to minumum of heat rejection of cooling tower and heat consumption of DHW system
for i in range(len(y1)):
    t1s.append(t1[i])
    y1s.append(y1[i])
    y2s.append(y2[i])
    y3s.append(y3[i])
    if i%2==0:
        if y1s[i]<y3s[i]:
            t7s.append(t1s[i])
            y7s.append(y1s[i])
        else:
            t7s.append(t1s[i])
            y7s.append(y3s[i])
        if t1s[i]%3600==0:
            t1d.append(t1s[i])
            y1d.append(y1s[i]/1000)
            y2d.append(-y2s[i]/1000)
            y3d.append(y3s[i]/1000)
            
for i in range(len(t1d)-2):
    t1d1.append(t1d[i])
    y1d1.append(y1d[i])
    y2d1.append(y2d[i])
    y3d1.append(y3d[i])
    
    if y1d1[i]<y3d1[i]:
        y4d1.append(y1d1[i])
    else:
        y4d1.append(y3d1[i])
        

y1d2=np.sum(np.asarray(y1d1).reshape(365,24),axis=1)
#y1d3=np.sum(y1d2,axis=1)
#y2d2=np.asarray(y2d1).reshape(365,24)
y2d2=np.sum((np.asarray(y2d1).reshape(365,24)),axis=1)
#y3d2=np.asarray(y3d1).reshape(365,24)
y3d2=np.sum((np.asarray(y3d1).reshape(365,24)),axis=1) 

for i in range(len(y1d2)):
    if y1d2[i]<y3d2[i]:
        y4d2.append(y1d2[i])
    else:
        y4d2.append(y3d2[i])
        
y4d2=np.sum((np.asarray(y4d1).reshape(365,24)),axis=1)       

plt.plot(y1d2,label='Heat rejection of cooling tower',lw=2.5)
plt.plot(y2d2,label='Actual Heat recovery',lw=2.5)
plt.plot(y3d2,label='Heat consumption of DHW systme in baseline model',lw=2.5)
plt.plot(y4d2, label='Ideal heat rejection',lw=2.5)
#plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
           ncol=1, mode="expand", borderaxespad=0.)
plt.annotate('41.9%',xy=(180,1000),xytext=(180,1000))
plt.annotate('83%',xy=(180,3500),xytext=(180,3500))
plt.xlabel('Time [d]')
plt.ylabel('Heat [kWh]')
#Fig = plt.gcf()
#Fig.set_size_inches(10, 6)
'''
from buildingspy.io.outputfile import Reader
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from scipy.interpolate import interp1d

r=Reader('HotelModel_annual.mat','dymola')
r1=Reader('HotelModel_annual_baseline1.mat','dymola')

(t1,y1)=r.values('coolingTowerSystem.coolingTower.yorkCalc.Q_flow') # Heat rejection of cooling tower negative value
(t2,y2)=r.values('heatPump.HexVal.Hex.hex.Q1_flow') # Heat recovery in advanced system  negative value
(t3,y3)=r1.values('domesticWaterControls.domesticHotWaterSystem.boi.QWat_flow') # Heat consumption of DHW systme without heat recovery
#(t4,y4)=r.values('CT_rej_Tot.y') # Total Heat rejection of cooling tower
#(t5,y5)=r.values('HeaRecTot.y') # Total heat recovery in advanced system
#(t6,y6)=r1.values('HeaConDHWTot.y') # Total Heat consumption of DHW systme  
#for i in range(len(t3)):
f1=interp1d(t3,y3,kind='linear')
y4=f1(t1) # interpolate y3 to make it has the same timestep with t1
y5=[] # ideal heat recovery
for i in range(len(y1)):
    if (-y1[i])>y4[i]:
        y5.append(y4[i])
    else:
        y5.append(-y1[i])
E_CT=-r.integral('coolingTowerSystem.coolingTower.yorkCalc.Q_flow')
E_HR=-r.integral('heatPump.HexVal.Hex.hex.Q1_flow')
E_HR_IDEAL=0
for i in range(len(y1)-1):
    E_HR_IDEAL=E_HR_IDEAL+(t1[i+1]-t1[i])*y5[i]
print E_HR_IDEAL
print E_CT
print E_HR
    
ratio_ideal=E_HR_IDEAL/E_CT
ratio_actual=E_HR/E_CT
        
plt.plot(-y1,lw=0.5)
#plt.plot(y3)
#plt.plot(y5,lw=0.5)
plt.plot(-y2,lw=0.5)
Fig = plt.gcf()
Fig.set_size_inches(10, 4)
    
#plt.plot(t1)      
#plt.plot(t3) 
#plt.plot(y3)    
            
