# -*- coding: utf-8 -*-
"""
Created on Sun Sep 24 18:26:16 2017

@author: Danlin
"""

import pandas as pd
import matplotlib.pyplot as plt

ax1 = plt.subplot(1,2,1)
s=pd.Series([14814.0,37604.0,2849.9],index=["Power(kW⋅h)","Gas($m^3$)","Water($m^3$)"])
# Set descriptions:
plt.ylabel('Energy Saving')
#Set tick colors:
ax = plt.gca()
ax.tick_params(axis='x', colors='blue')
ax.tick_params(axis='y', colors='red')   
#Plot the data:
my_colors = 'ryb'  #red, green, blue, black, etc.
s.plot(
    kind='bar', 
    color=my_colors,
)
plt.text(-0.25,15500,r'14814.0')
plt.text(0.75,38100,r'37604.0')
plt.text(1.75,3300,r'2849.9')
Fig = plt.gcf()
Fig.set_size_inches(6, 5.5)
plt.xticks(rotation=0)

ax2 = plt.subplot(1,2,2)
s=pd.Series([5.3,16.9,43.5],index=["Power(kW⋅h)","Gas($m^3$)","Water($m^3$)"])
# Set descriptions:
plt.ylabel('Energy Saving Ratio [%]')
#Set tick colors:
ax = plt.gca()
ax.tick_params(axis='x', colors='blue')
ax.tick_params(axis='y', colors='red')   
#Plot the data:
my_colors = 'ryb'  #red, green, blue, black, etc.
s.plot(
    kind='bar', 
    color=my_colors,
)

plt.text(-0.1,6,r'5.3')
plt.text(0.85,17.6,r'16.9')
plt.text(1.85,44.2,r'43.5')
Fig = plt.gcf()
Fig.set_size_inches(10, 6)
plt.xticks(rotation=0)


plt.show()