# -*- coding: utf-8 -*-
"""
Created on Sun Sep 24 18:26:16 2017

@author: Danlin
"""

import pandas as pd
import matplotlib.pyplot as plt

s=pd.Series([0,0,0,1.24,20.61,78.15,0],index=[1,2,3,4,5,6,7])

# Set descriptions:
plt.ylabel('Frequency [%]')
plt.xlabel('State')   

#Set tick colors:
ax = plt.gca()
ax.tick_params(axis='x', colors='blue')
ax.tick_params(axis='y', colors='red')   

#Plot the data:
my_colors = 'rrrrybb'  #red, green, blue, black, etc.

s.plot(
    kind='bar', 
    color=my_colors,
)
plt.text(-0.25,2,r'0.00')
plt.text(0.75,2,r'0.00')
plt.text(1.75,2,r'0.00')
plt.text(2.75,3,r'1.24')
plt.text(3.7,22,r'20.61')
plt.text(4.7,79,r'78.15')
plt.text(5.75,2,r'0.00')
Fig = plt.gcf()
Fig.set_size_inches(6, 4.5)
plt.show()