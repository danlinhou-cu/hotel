# -*- coding: utf-8 -*-
"""
Created on Sun Oct 01 20:35:28 2017

@author: Danlin
"""

import matplotlib.pyplot as plt
from pylab import *
import numpy as np
import matplotlib.dates as mdates
t=[1,2,3,4,5]
y1=[10,20,30,40,50]
y2=[2,4,6,8,10]
y3=[3,6,9,12,15]
y4=[0.05,0.1,0.15,0.2,0.25]

ax1=plt.scatter(t,y1,color='red',s=50,marker='o',label='Load')
ax2=plt.bar(t,y2,color='green',label='Heat Rejection of Cooling Tower')
ax3=plt.bar(t,y3,bottom=y2,color='yellow',label='Heat Utilization of DHW System')
plt.legend()
ax4=ax1,twinx()
ax4=plt.scatter(t,y4,color='blue',s=50,marker='o',label='State')
plt.legend()


plt.show()
'''
from buildingspy.io.outputfile import Reader
import matplotlib.pyplot as plt
from pylab import *
import numpy as np
import matplotlib.dates as mdates

r=Reader('HotelModel_annual.mat','dymola')
(t1,y1)=r.values('Load.y[1]')
(t2,y2)=r.values('supCon.y')
(t3,y3)=r.values('CT_rej.y')
(t4,y4)=r.values('HexVal_2.y')

t1d=[]
t2d=[]
t3d=[]
t4d=[]
y1d=[]
y2d=[]
y3d=[]
y4d=[]

for i in range(len(y1)):
    t1d.append(t1[i])
    y1d.append(y1[i])
    
for j in range(len(y2)):
    t2d.append(t2[j])
    y2d.append(y2[j])
    
for k in range(len(y3)):
    t3d.append(t3[k])
    y3d.append(y3[k])
    
for z in range(len(y4)):
    t4d.append(t1[4])
    y4d.append(y1[4])
  
ax1=plt.scatter(t1d,y1d,color='red',s=0.01,marker='o',label='Load')
ax2=plt.bar(t3d,y3d,color='green',label='Heat Rejection of Cooling Tower')

ax3=plt.bar(t4d,y4d,bottom=y3d,color='yellow',label='Heat Utilization of DHW System')
plt.legend()
ax4=ax1,twinx()
ax4=plt.scatter(t2d,y2d,color='blue',s=50,marker='o',label='State')
plt.legend()

Fig = plt.gcf()
Fig.set_size_inches(20, 6)
plt.show()
'''
