import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from buildingspy.io.outputfile import Reader
import csv

r=Reader('HotelModel_annual.mat','dymola')
(t1,y1)=r.values('load.Loa')
(t2,y2)=r.values('CT_rej.y')
(t3,y3)=r.values('coolingTowerSystem.coolingTower.yorkCalc.TLvg')
(t4,y4)=r.values('domesticWaterControls.domesticHotWaterSystem.boi.QFue_flow')
(t5,y5)=r.values('GueRooDomWatDem.y[1]')
(t6,y6)=r.values('supCon.y')

t1d=[]
t2d=[]
t3d=[]
t4d=[]
t5d=[]
y1d=[]
y2d=[]
y3d=[]
y4d=[]
y5d=[]
t6d=[]
y6d=[]

for i in range(len(y1)):
   if t1[i]>259200 and t1[i]<345600:
    t1d.append(t1[i]-259200)
    y1d.append(y1[i])
for j in range(len(y2)):
   if t2[j]>259200 and t2[j]<345600:
    t2d.append(t2[j]-259200)
    y2d.append(y2[j])
for k in range(len(y3)):
   if t3[k]>259200 and t3[k]<345600:
    t3d.append(t3[k]-259200)
    y3d.append(y3[k])
for z in range(len(y4)):
   if t4[z]>259200 and t4[z]<345600:
    t4d.append(t4[z]-259200)
    y4d.append(y4[z]-273.15)
for a in range(len(y5)):
   if t5[a]>259200 and t5[a]<345600:
    t5d.append(t5[a]-259200)
    y5d.append(y5[a])
for b in range(len(y6)):
   if t6[b]>259200 and t6[b]<345600:
    t6d.append(t6[b]-259200)
    y6d.append(y6[b])


xlabind=[]
xlab=[]
x=(t1d[len(t1d)-1]-t1d[0])/3600
for i in range(int(round(x+1))):
   xlabind.append(t1d[0]+3600*i)
   xlab.append(i)
print xlabind
print xlab

ax1 = plt.subplot(6,1,1)
ax1.plot(t1d,y1d,color='black')
#ax1.plot(t2d,y2d,lable="DHW Boiler Heat [w]")
#ax1.plot(t3d,y3d,lable="Rejection Heat of Cooling Tower [w]")
#plt.legend()
ax1.set_ylabel('Load [w]')
#ylim(0,8)
xtic=[]
#xticks(xtic,'')

ax2 = plt.subplot(6,1,2)
ax2.plot(t2d,y2d,color='black')
ax2.set_ylabel('Rejection Heat of Cooling Tower [w]')
#ylim(0,8)
xtic=[]
#xticks(xtic,'')

ax3 = plt.subplot(6,1,3)
ax3.plot(t3d,y3d,color='black')
ax3.set_ylabel('Leaving Temperature of Cooling Tower [$^\circ$C)]')
#ylim(0,8)
xtic=[]
#xticks(xtic,'')

ax4 = plt.subplot(6,1,4)
ax4.plot(t4d,y4d,color='black')
ax4.set_ylabel('DHW Boiler Heat [w]')
#ylim(0,8)
xtic=[]
#xticks(xtic,'')

ax5 = plt.subplot(6,1,5)	
ax5.plot(t5d,y5d,color='black')
ax5.set_ylabel('DHW Usage')
xtic=[]
#xticks(xtic,'')

ax6 = plt.subplot(6,1,6)	
ax6.plot(t6d,y6d,color='black')
ax6.set_ylabel('State')
ax6.set_xlabel('JAN 4th')
xtic=[]
#xticks(xtic,'')


#ylim(0,8)
#xtic=[5]
#xticks(xtic,'')

Fig = plt.gcf()
Fig.set_size_inches(10, 20)
#plt.xticks(np.arange(t1[0],t1[189070-1],7877),[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24])
   
plt.show()				 





