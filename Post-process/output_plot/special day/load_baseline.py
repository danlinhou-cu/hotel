import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from buildingspy.io.outputfile import Reader
import csv

r=Reader('HotelModel_annual.mat','dymola')
(t1,y1)=r.values('load.Loa')

t1d=[]
y1d=[]
for i in range(len(y1)):
    t1d.append(t1[i])
    y1d.append(y1[i])
   
f = open('load_baseline.csv', 'w')    
columnTitleRow="Time(s),Load(w)\n"
f.write(columnTitleRow)
ini=0
for i in range(len(t1d)):
    f.writelines('       '+str(t1d[i])+'     ,     '+str(y1d[i])+'\n')
    ini=ini+1
    
f.close()


table=pd.read_csv('load_baseline.csv')

table=table.drop_duplicates(subset=['Time(s)'], keep='first')

			 
f=open('load_baseline2.txt','w')
f.writelines('#1'+'\n')	

f.writelines('double tab1('+str(len(table))+',2)   # comment line			 '+'\n')	
ini=0
for i in range(len(table)):
	    f.writelines('       '+str(table['Time(s)'].iloc[i])+'          '+str(table['Load(w)'].iloc[i])+'\n')
	    ini=ini+1		

f.close()




